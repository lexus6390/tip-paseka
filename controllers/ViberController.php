<?php

namespace app\controllers;

use app\services\ViberBot\ViberBot;
use yii\web\Controller;

/**
 * Class ViberController
 * @package app\controllers
 */
class ViberController extends Controller
{
    public function beforeAction($action)
    {
        if($action->id === 'bot'){
            \Yii::$app->controller->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }


    /**
     * @return void
     */
    public function actionBot()
    {
        $viberBotService = new ViberBot();
        $viberBotService->setViberBot();
    }
}