<?php

namespace app\controllers;

use app\models\Article;
use app\models\DatabaseCategory;
use app\models\Feedback;
use app\models\MainPageInfo;
use app\models\ProductCard;
use app\services\MailNotifications\MailNotificationManager;
use app\services\ViberBot\ViberSetWebhook;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\HttpException;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Страница-заглушка для вывода ошибок
     * @return string
     */
    public function actionError()
    {
        /* @var HttpException $exception */
        $exception = \Yii::$app->errorHandler->exception;
        if ($exception !== null) {
            $statusCode = $exception->statusCode;
            $name = $exception->getName();
            $message = $exception->getMessage();
            return $this->render('error', [
                'exception' => $exception,
                'statusCode' => $statusCode,
                'name' => $name,
                'message' => $message
            ]);
        }
    }

    /**
     * Главная страница
     *
     * @return string
     */
    public function actionIndex()
    {
        $mainPageInfo = MainPageInfo::find()->all();

        return $this->render('index', [
            'mainPageInfo' => $mainPageInfo[0]
        ]);
    }

    /**
     * Страница категорий продуктов
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProducts()
    {
        $products = ProductCard::find()
            ->where(['is_visible' => 1])
            ->orderBy('priority ASC')
            ->all();

        if(empty($products)) {
            throw new NotFoundHttpException('Не найдено ни одного продукта');
        }

        return $this->render('products', [
            'products' => $products
        ]);
    }

    /**
     * Страница карточки продукта
     *
     * @param $id int
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionProductDetail($id)
    {
        $product = ProductCard::findOne(['id' => $id]);

        if(is_null($product)) {
            throw new NotFoundHttpException('Продукт не найден');
        }

        return $this->render('product-detail-new', [
            'product' => $product
        ]);
    }

    /**
     * Страница категорий статей
     * Главная страницы базы знаний
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionDatabase()
    {
        $categories = DatabaseCategory::find()
            ->orderBy('priority ASC')
            ->all();

        if(empty($categories)) {
            throw new NotFoundHttpException('Не найдено ни одного раздела статей');
        }

        // Не выводим категории, в которых нет ни одной статьи
        $resultCategories = [];
        foreach ($categories as $id => $category) {
            if(!empty($category->articles)) {
                $resultCategories[] = $category;
            }
        }

        return $this->render('database', [
            'categories' => $resultCategories
        ]);
    }

    /**
     * Страница выбранной категории статей в базе знаний
     *
     * @param int $id`
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionArticles($id)
    {
        $articles = Article::find()
            ->where([
                'category_id'  => $id,
                'is_published' => 1
            ])
            ->orderBy('priority ASC')
            ->all();

        if(empty($articles)) {
            throw new NotFoundHttpException('Не найдено ни одной статьи в выбранном разделе');
        }

        return $this->render('articles', [
            'articles' => $articles
        ]);
    }

    /**
     * Страница выбранной статьи в базе знаний
     *
     * @param int $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionNote($id)
    {
        $article = Article::findOne([
            'id' => $id,
            'is_published' => 1
        ]);

        if(is_null($article)) {
            throw new NotFoundHttpException('Статья не найдена');
        }

        return $this->render('note', [
            'article' => $article
        ]);
    }

    /**
     * Страница контактов
     *
     * @return string
     */
    public function actionContact()
    {
        $feedback = new Feedback();

        if(\Yii::$app->request->isAjax) {
            $feedback->setAttributes([
                'name' => $_POST['name'],
                'last_name' => $_POST['last_name'],
                'email'     => $_POST['email'],
                'theme'     => $_POST['theme'],
                'message'   => $_POST['message']
            ]);

            if(!$feedback->validate()) {
                return $this->asJson(['errors' => implode('<br> ', ArrayHelper::getColumn($feedback->getErrors(), 0))]);
            }

            if($feedback->save()) {

                // Отправка уведомления на почту
                $mailService = new MailNotificationManager('FeedbackNotification', $feedback, \Yii::$app->params['mail_to']);
                $mailService->sendEmail();

                return $this->asJson([
                    'success' => true,
                    'link'    => \Yii::$app->urlManager->hostInfo.'/contact?is_send=1'
                ]);
            }
        }

        return $this->render('contact', [
            'feedback'     => $feedback,
            'mainPageInfo' => MainPageInfo::find()->all()[0]
        ]);
    }

    /**
     * Страница корзины
     *
     * @return string
     */
    public function actionCart()
    {
        return $this->render('cart');
    }

    /**
     *
     */
    public function actionViber()
    {
        $viberService = new ViberSetWebhook();
        $viberService->setWebHook();
    }
}
