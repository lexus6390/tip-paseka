<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $feedback \app\models\Feedback */

?>
<div class="password-reset">
    <h2>Поступило новое сообщение в обратную связь!</h2>
    <p>
        <b>Имя:</b> <?= Html::encode($feedback->name) ?>
    </p>
    <p>
        <b>Фамилия:</b> <?= Html::encode($feedback->last_name) ?>
    </p>
    <p>
        <b>Email:</b> <?= Html::encode($feedback->email) ?>
    </p>
    <p>
        <b>Тема обращения:</b> <?= Html::encode($feedback->theme) ?>
    </p>
    <p>
        <b>Текст сообщения:</b> <?= Html::encode($feedback->message) ?>
    </p>
    <p>
        <b>Дата и время отправки сообщения:</b> <?= date('Y-m-d H:i:s') ?>
    </p>
</div>
