<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $user \app\models\AdminUser */

$resetLink = \Yii::$app->urlManager->createAbsoluteUrl(['admin/default/reset-password', 'token' => $user->reset_token]);
?>
<div class="password-reset">
    <p>Здравствуйте, <?= Html::encode($user->login) ?>,</p>

    <p>Следуйте приведенной ниже ссылке, чтобы сменить пароль:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
