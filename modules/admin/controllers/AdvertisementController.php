<?php

namespace app\modules\admin\controllers;

use app\models\AdvertisementClient;
use app\services\ViberBot\ViberBot;
use Yii;
use app\models\Advertisement;
use app\models\search\AdvertisementSearch;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * AdvertisementController implements the CRUD actions for Advertisement model.
 */
class AdvertisementController extends BaseController
{
    /**
     * Lists all Advertisement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdvertisementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Advertisement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Advertisement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Advertisement();

        if ($model->load(Yii::$app->request->post())) {

            $model->save();
            $image = UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->picture = $this->saveImage($model, $image);
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Advertisement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $image =  UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->picture = $this->saveImage($model, $image);
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Advertisement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        /** @var Advertisement $model */
        $model = $this->findModel($id);
        if(is_file(Yii::getAlias('@webroot').$model->picture)) {
            unlink(Yii::getAlias('@webroot').$model->picture);
        }

        AdvertisementClient::deleteAll(['advertisement_id' => $model->id]);

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Отправка рекламного сообщения
     *
     * @param $id
     * @return \yii\web\Response
     * @throws BadRequestHttpException
     */
    public function actionSend($id)
    {
        /** @var Advertisement $model */
        $model = $this->findModel($id);

        if($model->is_draft) {
            throw new BadRequestHttpException('Ошибка. Объявление в статусе черновика');
        }
        if($model->is_send) {
            throw new BadRequestHttpException('Ошибка. Объявление уже отправлено ранее');
        }

        // Отправка рекламы всем подписчикам бота
        $viberBot = new ViberBot();
        if($viberBot->sendMessage($model)) {
            $model->is_send = 1;
            $model->send_at = date('Y-m-d H:i:s');
            if($model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Advertisement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Advertisement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Advertisement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Сохранение изображения
     * @param $model Advertisement
     * @param $image UploadedFile
     * @return string
     */
    private function saveImage($model, $image)
    {
        $path = '/images/advertisement/'.$model->id.'_'.\Yii::$app->security->generateRandomString(8).'.'.$image->extension;

        if(!is_dir(\Yii::getAlias('@webroot').'/images/advertisement')) {
            mkdir(\Yii::getAlias('@webroot').'/images/advertisement');
        }
        $image->saveAs(\Yii::getAlias('@webroot').$path);
        return $path;
    }
}
