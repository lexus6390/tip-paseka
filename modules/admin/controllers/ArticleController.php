<?php

namespace app\modules\admin\controllers;

use richardfan\sortable\SortableAction;
use Yii;
use app\models\Article;
use app\models\search\ArticleSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ArticleController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function actions(){
        return [
            'sortItem' => [
                'class' => SortableAction::class,
                'activeRecordClassName' => Article::class,
                'orderColumn' => 'priority',
            ],
            'images-get' => [
                'class' => 'vova07\imperavi\actions\GetImagesAction',
                'url' => '/images/article_images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@webroot').'/images/article_images/', // Or absolute path to directory where files are stored.
            ],
            'image-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/images/article_images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@webroot').'/images/article_images/', // Or absolute path to directory where files are stored.
            ],
            'file-delete' => [
                'class' => 'vova07\imperavi\actions\DeleteFileAction',
                'url' => '/images/article_images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@webroot').'/images/article_images/', // Or absolute path to directory where files are stored.
            ],
            'files-get' => [
                'class' => 'vova07\imperavi\actions\GetFilesAction',
                'url' => '/images/article_images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@webroot').'/images/article_images/', // Or absolute path to directory where files are stored.
            ],
            'file-upload' => [
                'class' => 'vova07\imperavi\actions\UploadFileAction',
                'url' => '/images/article_images/', // Directory URL address, where files are stored.
                'path' => \Yii::getAlias('@webroot').'/images/article_images/', // Or absolute path to directory where files are stored.
                'uploadOnlyImage' => false, // For any kind of files uploading.
            ]
        ];
    }

    /**
     * Lists all Article models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ArticleSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Article model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Article model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Article();

        if ($model->load(Yii::$app->request->post())) {
            $model->priority = $this->getNumberLastRecord();
            $model->save();
            $image = UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'article');
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Article model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image =  UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'article');
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Article model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        /** @var Article $model */
        $model = $this->findModel($id);
        if(is_file(Yii::getAlias('@webroot').$model->link)) {
            unlink(Yii::getAlias('@webroot').$model->link);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Article model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Article the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Article::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Сохранение изображения
     * @param $model Article
     * @param $image UploadedFile
     * @param $type string
     * @return string
     */
    private function saveImage($model, $image, $type)
    {
        $path = '/images/article/'.$model->id.'_'.$type.'.'.$image->extension;

        if(!is_dir(\Yii::getAlias('@webroot').'/images/article')) {
            mkdir(\Yii::getAlias('@webroot').'/images/article');
        }
        $image->saveAs(\Yii::getAlias('@webroot').$path);
        return $path;
    }

    /**
     * Получение номера приоритетности для новой записи
     * @return int|string
     */
    private function getNumberLastRecord()
    {
        return Article::find()->count() + 1;
    }
}
