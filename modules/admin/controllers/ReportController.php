<?php

namespace app\modules\admin\controllers;

use app\models\forms\ReportClientFilterForm;
use app\models\forms\ReportFilterForm;
use app\models\search\OrderDetailSearch;

/**
 * Class ReportController
 * @package app\modules\admin\controllers
 */
class ReportController extends BaseController
{
    /**
     * Отчет по продуктам
     *
     * @return string
     */
    public function actionProduct()
    {
        $reportFilterForm = new ReportFilterForm();

        $searchModel = new OrderDetailSearch();

        if(\Yii::$app->request->get()) {
            $reportFilterForm->load(\Yii::$app->request->get());
        }
        $dataProvider = $searchModel->searchForReport(\Yii::$app->request->queryParams, $reportFilterForm->dateStart, $reportFilterForm->dateEnd);

        return $this->render('product', [
            'reportFilterForm' => $reportFilterForm,
            'searchModel'      => $searchModel,
            'dataProvider'     => $dataProvider,
        ]);
    }

    /**
     * Отчет по клиенту
     *
     * @return string
     */
    public function actionClient()
    {
        $reportFilterForm = new ReportClientFilterForm();

        $searchModel = new OrderDetailSearch();

        if(\Yii::$app->request->get()) {
            $reportFilterForm->load(\Yii::$app->request->get());

        }
        $dataProvider = $searchModel->searchForClientReport(\Yii::$app->request->queryParams, $reportFilterForm->client, $reportFilterForm->dateStart, $reportFilterForm->dateEnd);



        return $this->render('client', [
            'reportFilterForm' => $reportFilterForm,
            'searchModel'      => $searchModel,
            'dataProvider'     => $dataProvider,
            'issetClientId'    => isset($_GET['ReportClientFilterForm']['client'])
        ]);
    }
}