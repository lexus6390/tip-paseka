<?php

namespace app\modules\admin\controllers;

use app\models\AdminUser;
use app\models\forms\LoginForm;
use app\models\forms\RequestChangePasswordForm;
use app\models\forms\ResetPasswordForm;
use app\modules\admin\services\ChangePasswordService;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends BaseController
{
    public $layout = 'login';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->layout = 'admin';

        return $this->redirect(Url::toRoute('main-page-info/index'));
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect('/admin/default/index');
        }

        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->redirect('/admin/default/index');
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        \Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Страница запроса смены пароля администратора
     * @return string
     */
    public function actionRequestChangePassword()
    {
        if(\Yii::$app->request->post()) {
            $changeService = new ChangePasswordService();

            if($isSend = $changeService->sendMessageForUser($_POST['RequestChangePasswordForm']['login'])) {
                $email = $_POST['RequestChangePasswordForm']['login'];
            }
        }

        return $this->render('request-change-password', [
            'model'  => new RequestChangePasswordForm(),
            'isSend' => isset($isSend) ? $isSend : null,
            'email'  => isset($email) ? $email : null
        ]);
    }

    /**
     * Создание нового пароля для администратора
     * @return string|Response
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function actionResetPassword()
    {
        $token = \Yii::$app->request->get('token');

        if(is_null($token)) {
            throw new BadRequestHttpException('Отсутствует обязательный параметр token');
        }

        $existToken = AdminUser::findOne(['reset_token' => $token]);

        if(is_null($existToken)) {
            throw new NotFoundHttpException('Отсутствует пользователь с переданным токеном');
        }

        if(\Yii::$app->request->post()) {
            $changeService = new ChangePasswordService();
            if($changeService->changePassword($_POST['ResetPasswordForm']['password'])) {
                return $this->redirect('index');
            } else {
                $error = true;
            }
        }

        return $this->render('reset-password', [
            'model' => new ResetPasswordForm(),
            'error' => isset($error) ? $error : null
        ]);
    }
}
