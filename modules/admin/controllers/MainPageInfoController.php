<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\MainPageInfo;
use yii\web\NotFoundHttpException;

/**
 * MainPageInfoController implements the CRUD actions for MainPageInfo model.
 */
class MainPageInfoController extends BaseController
{
    /**
     * Lists all MainPageInfo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $mainPageInfo = MainPageInfo::find()->all();

        return $this->render('index', [
            'model' => $mainPageInfo[0]
        ]);
    }

    /**
     * Updates an existing MainPageInfo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the MainPageInfo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MainPageInfo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MainPageInfo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
