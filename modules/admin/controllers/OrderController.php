<?php

namespace app\modules\admin\controllers;

use app\models\Client;
use app\models\OrderDetail;
use app\models\ProductCard;
use app\services\Order\AddOrderDetail;
use app\services\Order\UpdateOrderDetail;
use Yii;
use app\models\Order;
use app\models\search\OrderSearch;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends BaseController
{
    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ServerErrorHttpException
     */
    public function actionCreate()
    {
        $model = new Order();

        if ($model->load(Yii::$app->request->post())) {

            $model->products = Yii::$app->request->post('Order')['products'];
            if($model->products == '') {
                $model->addError('products', 'Надо бы заполнить');
                return $this->render('create', [
                    'model'    => $model,
                    'clients'  => Client::getClientList(),
                    'products' => ProductCard::getProductList()
                ]);
            }
            $model->total_price = 0;
            if(!$model->save()) {
                throw new ServerErrorHttpException('Ошибка сохранения заказа');
            }

            $orderDetailService = new AddOrderDetail();
            $orderDetailService->addOrderDetail($model);

            $model->total_price = $orderDetailService->totalPrice;
            if(!$model->save()) {
                throw new ServerErrorHttpException('Ошибка сохранения заказа');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model'    => $model,
            'clients'  => Client::getClientList(),
            'products' => ProductCard::getProductList()
        ]);
    }

    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param $id
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionUpdate($id)
    {
        /** @var Order $model */
        $model = $this->findModel($id);
        $model->getProductDetails();

        if ($model->load(Yii::$app->request->post())) {

            $model->products = Yii::$app->request->post('Order')['products'];
            if($model->products == '') {
                $model->addError('products', 'Надо бы заполнить');
                return $this->render('create', [
                    'model'    => $model,
                    'clients'  => Client::getClientList(),
                    'products' => ProductCard::getProductList()
                ]);
            }

            $orderDetailService = new UpdateOrderDetail();
            $orderDetailService->updateOrderDetail($model);

            $model->total_price = $model->updateTotalPrice();

            if(!$model->save()) {
                throw new ServerErrorHttpException('Ошибка сохранения заказа');
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model'    => $model,
            'clients'  => Client::getClientList(),
            'products' => ProductCard::getProductList()
        ]);
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        OrderDetail::deleteAll(['order_id' => $id]);
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
