<?php

namespace app\modules\admin\controllers;

use richardfan\sortable\SortableAction;
use Yii;
use app\models\DatabaseCategory;
use app\models\search\DatabaseCategorySearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * DatabaseCategoryController implements the CRUD actions for DatabaseCategory model.
 */
class DatabaseCategoryController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function actions(){
        return [
            'sortItem' => [
                'class' => SortableAction::class,
                'activeRecordClassName' => DatabaseCategory::class,
                'orderColumn' => 'priority',
            ]
        ];
    }

    /**
     * Lists all DatabaseCategory models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DatabaseCategorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DatabaseCategory model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DatabaseCategory model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DatabaseCategory();

        if ($model->load(Yii::$app->request->post())) {
            $model->priority = $this->getNumberLastRecord();
            $model->save();
            $image = UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'category');
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DatabaseCategory model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
            $image =  UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'category');
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DatabaseCategory model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        /** @var DatabaseCategory $model */
        $model = $this->findModel($id);
        if(is_file(Yii::getAlias('@webroot').$model->link)) {
            unlink(Yii::getAlias('@webroot').$model->link);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DatabaseCategory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DatabaseCategory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DatabaseCategory::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Сохранение изображения
     * @param $model DatabaseCategory
     * @param $image UploadedFile
     * @param $type string
     * @return string
     */
    private function saveImage($model, $image, $type)
    {
        $path = '/images/category/'.$model->id.'_'.$type.'.'.$image->extension;

        if(!is_dir(\Yii::getAlias('@webroot').'/images/category')) {
            mkdir(\Yii::getAlias('@webroot').'/images/category');
        }
        $image->saveAs(\Yii::getAlias('@webroot').$path);
        return $path;
    }

    /**
     * Получение номера приоритетности для новой записи
     * @return int|string
     */
    private function getNumberLastRecord()
    {
        return DatabaseCategory::find()->count() + 1;
    }
}
