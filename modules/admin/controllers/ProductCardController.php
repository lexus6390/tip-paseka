<?php

namespace app\modules\admin\controllers;

use richardfan\sortable\SortableAction;
use Yii;
use app\models\ProductCard;
use app\models\search\ProductCardSearch;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * ProductCardController implements the CRUD actions for ProductCard model.
 */
class ProductCardController extends BaseController
{
    /**
     * {@inheritdoc}
     */
    public function actions(){
        return [
            'sortItem' => [
                'class' => SortableAction::class,
                'activeRecordClassName' => ProductCard::class,
                'orderColumn' => 'priority',
            ]
        ];
    }

    /**
     * Lists all ProductCard models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductCardSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductCard model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ProductCard model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductCard();

        if ($model->load(Yii::$app->request->post())) {
            if($model->info != '') {
                $model->info = json_encode($model->info, JSON_UNESCAPED_UNICODE);
            } else {
                $model->info = null;
            }
            $model->priority = $this->getNumberLastRecord();
            if($model->database_category_id == 0) {
                $model->database_category_id = null;
            }
            $model->save();
            $image = UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'product');
            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductCard model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->info != '') {
                $model->info = json_encode($model->info, JSON_UNESCAPED_UNICODE);
            } else {
                $model->info = null;
            }
            if($model->database_category_id == 0) {
                $model->database_category_id = null;
            }

            $image =  UploadedFile::getInstance($model, 'image');
            if(!is_null($image)) {
                $model->link = $this->saveImage($model, $image, 'product');
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $model->info = json_decode($model->info);

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductCard model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        /** @var ProductCard $model */
        $model = $this->findModel($id);
        if(is_file(Yii::getAlias('@webroot').$model->link)) {
            unlink(Yii::getAlias('@webroot').$model->link);
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProductCard model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductCard the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProductCard::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Сохранение изображения
     * @param $model ProductCard
     * @param $image UploadedFile
     * @param $type string
     * @return string
     */
    private function saveImage($model, $image, $type)
    {
        $path = '/images/product/'.$model->id.'_'.$type.'.'.$image->extension;

        if(!is_dir(\Yii::getAlias('@webroot').'/images/product')) {
            mkdir(\Yii::getAlias('@webroot').'/images/product');
        }
        $image->saveAs(\Yii::getAlias('@webroot').$path);
        return $path;
    }

    /**
     * Получение номера приоритетности для новой записи
     * @return int|string
     */
    private function getNumberLastRecord()
    {
        return ProductCard::find()->count() + 1;
    }
}
