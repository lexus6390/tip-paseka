<?php

namespace app\modules\admin\controllers;

use Yii;
use app\models\Feedback;
use app\models\search\FeedbackSearch;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

/**
 * FeedbackController implements the CRUD actions for Feedback model.
 */
class FeedbackController extends BaseController
{
    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Проставление записи обратной связи статуса "Прочитано"
     *
     * @param $id
     * @return \yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionRead($id)
    {
        $model = $this->findModel($id);
        $model->is_read = 1;
        if(!$model->save()) {
            throw new ServerErrorHttpException('Ошибка сохранения записи обратной связи');
        }
        return $this->redirect(['index']);
    }


    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
