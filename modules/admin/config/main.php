<?php

use app\models\AdminUser;

return [
    'name' => 'Пасека Типочкиных',
    'components' => [
        'user' => [
            'class' => 'yii\web\User',
            'identityClass' => AdminUser::class,
            'enableAutoLogin' => false,
            'loginUrl' => ['admin/default/login'],
            'identityCookie' => ['name' => 'admin', 'httpOnly' => true],
            'idParam' => 'admin_id',
        ]
    ],
    'params' => [
        // list of parameters
    ],
];
