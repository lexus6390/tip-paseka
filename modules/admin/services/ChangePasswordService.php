<?php

namespace app\modules\admin\services;

use app\models\AdminUser;
use app\services\MailNotifications\MailNotificationManager;
use yii\web\NotFoundHttpException;

/**
 * Сервис для реализации смены пароля администратора
 */
class ChangePasswordService
{
    /**
     * Отправка письма на почту для сброса пароля администратора
     * @param string $email
     * @return bool
     * @throws NotFoundHttpException
     */
    public function sendMessageForUser($email)
    {
        /** @var AdminUser $user */
        $user = AdminUser::findOne(['login' => $email]);

        if(is_null($user)) {
            throw new NotFoundHttpException('Пользователь с указанным e-mail не найден');
        }

        $user->reset_token = \Yii::$app->security->generateRandomString() . '_' . time();
        $user->save();

        $mailService = new MailNotificationManager('ResetPasswordNotification', $user, $email);

        return $mailService->sendEmail() ? true : false;
    }

    /**
     * Создание нового пароля для администратора
     * @param string $newPassword
     * @return bool
     * @throws NotFoundHttpException
     */
    public function changePassword($newPassword)
    {
        $token = \Yii::$app->request->get('token');

        /** @var AdminUser $user */
        $user = AdminUser::findOne(['reset_token' => $token]);

        if(is_null($user)) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        $user->secret = \Yii::$app->security->generatePasswordHash($newPassword);
        $user->reset_token = null;
        $user->save();
        return true;
    }
}