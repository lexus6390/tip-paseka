<?php

use app\models\Article;
use richardfan\sortable\SortableGridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Статьи';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS

.ui-sortable-handle:hover {
    cursor: pointer;
}

CSS;
$this->registerCss($css);

?>
<div class="article-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить статью', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <b>Изображения показаны в порядке очередности показа. Для смены очередности перетаскивайте записи.</b>
    </p>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortUrl' => Url::to(['sortItem']),

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'category_id',
                'filter'    => \app\models\DatabaseCategory::getCategories(),
                'value'     => function($model) {
                    return $model->category->title;
                }
            ],
            'title',
            'author',
            [
                'attribute' => 'link',
                'filter'    => false,
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var Article $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '15%']);
                }
            ],
            'is_published:boolean',
            [
                'attribute' => 'published_date',
                'filter'    => false,
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
