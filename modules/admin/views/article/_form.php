<?php

use app\models\DatabaseCategory;
use kartik\file\FileInput;
use vova07\imperavi\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Article */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'category_id')->dropDownList(DatabaseCategory::getCategories()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                'table',
                'fontcolor',
                'fontfamily',
                'fontsize'
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'text')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'imageUpload' => Url::to(['article/image-upload']),
            'plugins' => [
                'fullscreen',
                'table',
                'fontcolor',
                'fontfamily',
                'fontsize',
                'imagemanager',
                'video'
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'is_published')->checkbox() ?>

    <?= $form->field($model, 'published_date')->widget(DatePicker::class, [
        'language' => 'ru',
        'dateFormat' => 'yyyy-MM-dd'
    ]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::class, [
        'options'       => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview'=>[
                $model->link
            ],
            'allowedFileExtensions' => ['jpg','gif','png'],
            'previewFileType'       => 'image',
            'initialPreviewAsData'  => true,
            'fileActionSettings'    => [
                'showUpload' => false,
                'showDrag'   => false,
            ]
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
