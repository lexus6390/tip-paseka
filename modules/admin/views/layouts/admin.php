<?php

/**
 * @var string $content
 * @var \yii\web\View $this
 */

use yii\helpers\Html;

$bundle = yiister\gentelella\assets\Asset::register($this);

?>
<?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta charset="<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
    <?php $this->beginBody(); ?>
    <div class="container body">

        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title text-center" style="border: 0;height:80px">
                        <a href="/" class="site_title" style="height:108%">
                            <div class="profile_pic" style="width:75%">
                                <img style="height: 100%;border-radius: inherit" src="/images/logo.png" alt="..." class="img-circle profile_img">
                            </div>
                        </a>
                    </div>
                    <div class="clearfix"></div>

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu" style="margin-top: 20px;">

                        <div class="menu_section">
                            <h3>Меню</h3>
                            <?=
                            \yiister\gentelella\widgets\Menu::widget(
                                [
                                    "items" => [
                                        //["label" => "Главная", "url" => ["/admin/default/index"], "icon" => "home"],
                                        ["label" => "Главная страница", "url" => ["/admin/main-page-info/index"], "icon" => "bank"],
                                        ["label" => "Пункты меню", "url" => ["/admin/menu-item/index"], "icon" => "database"],
                                        ["label" => "Слайдер", "url" => ["/admin/slider/index"], "icon" => "image"],
                                        ["label" => "Продукты", "url" => ["/admin/product-card/index"], "icon" => "bank"],
                                        [
                                            "label" => "Отчёты",
                                            "icon" => "table",
                                            "url" => "#",
                                            "items" => [
                                                ["label" => "Отчёт по продуктам", "url" => ["/admin/report/product"]],
                                                ["label" => "Отчёт по клиенту", "url" => ["/admin/report/client"]],
                                            ],
                                        ],
                                        ["label" => "Обратная связь", "url" => ["/admin/feedback/index"], "icon" => "refresh"],
                                        ["label" => "Категории статей", "url" => ["/admin/database-category/index"], "icon" => "newspaper-o"],
                                        ["label" => "Статьи", "url" => ["/admin/article/index"], "icon" => "mortar-board"],
                                        //["label" => "Заявки", "url" => ["/admin/request/index"], "icon" => "male"],
                                        ["label" => "Клиенты", "url" => ["/admin/client/index"], "icon" => "users"],
                                        ["label" => "Реклама", "url" => ["/admin/advertisement/index"], "icon" => "tripadvisor"],
                                        ["label" => "Заказы", "url" => ["/admin/order/index"], "icon" => "shopping-cart"],
                                    ]
                                ]
                            )
                            ?>
                        </div>

                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">

                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <?php if(!\Yii::$app->user->isGuest) { ?>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="http://placehold.it/128x128" alt="">
                                        <?= \Yii::$app->user->identity->login ?>
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li>
                                            <?= Html::a('<i class="fa fa-sign-out pull-right"></i> Выйти', \yii\helpers\Url::toRoute(['/admin/default/logout'])) ?>
                                        </li>
                                    </ul>
                                </li>

                                <li role="presentation" class="dropdown">
                                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                                        <i class="fa fa-envelope-o"></i>
                                    </a>
                                </li>

                            </ul>
                        <?php } ?>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->

            <!-- page content -->
            <div class="right_col" role="main">
                <?php if (isset($this->params['h1'])): ?>
                    <div class="page-title">
                        <div class="title_left">
                            <h1><?= $this->params['h1'] ?></h1>
                        </div>
                        <div class="title_right">
                            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search for...">
                                    <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="clearfix"></div>

                <?= $content ?>
            </div>
            <!-- /page content -->
        </div>

    </div>

    <div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
    <!-- /footer content -->
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>