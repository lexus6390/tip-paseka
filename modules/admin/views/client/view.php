<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */

$this->title = $model->getFullName();
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить этого клиента?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'middle_name',
            'last_name',
            'phone',
            'email:email',
            'has_viber:boolean',
            'viber_id',
            'viber_name',
            'created_at',
            'updated_at',
            [
                'attribute' => 'sended_advertising',
                'label' => 'Отправленная реклама',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\models\AdvertisementClient[] $advertisingsClient */
                    $advertisingsClient = $model->advertisementClients;

                    if(!empty($advertisingsClient)) {
                        $string = '';

                        foreach ($advertisingsClient as $item) {

                            $string .=  Html::a($item->advertisement->name, Url::toRoute(['advertisement/view', 'id' => $item->advertisement_id]));
                            $string .= "<br>";
                        }
                        return $string;
                    }

                    return null;
                }
            ],
            [
                'attribute' => 'orders',
                'label' => 'Заказы',
                'format' => 'raw',
                'value' => function($model) {
                    $orders = $model->orders;

                    if(!empty($orders)) {
                        $string = '';

                        foreach ($orders as $item) {

                            $string .=  Html::a($item->id, Url::toRoute(['order/view', 'id' => $item->id]));
                            $string .= ' - '.$item->season.'г., на сумму '.$item->total_price.'₽';
                            $string .= "<br>";
                        }
                        $string .= "<br>";
                        $string .= Html::a('Подробный отчёт...', Url::toRoute(['report/client', 'ReportClientFilterForm[client]' => $model->id]));
                        return $string;
                    }

                    return null;
                }
            ]
        ],
    ]) ?>

</div>
