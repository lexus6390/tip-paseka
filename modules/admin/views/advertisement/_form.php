<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Advertisement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="advertisement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::class, [
        'options'       => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview'=>[
                $model->picture
            ],
            'allowedFileExtensions' => ['jpg'],
            'previewFileType'       => 'image',
            'initialPreviewAsData'  => true,
            'fileActionSettings'    => [
                'showUpload' => false,
                'showDrag'   => false,
            ],
            'initialPreviewConfig' =>  [
                [
                    'caption' => $model->picture,
                    'url'     => Url::toRoute(['advertisement/delete-image']),
                    'key'     => $model->id,

                ]
            ],
        ],
    ]) ?>

    <?= $form->field($model, 'is_draft')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
