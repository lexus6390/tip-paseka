<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\AdvertisementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Реклама';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advertisement-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать рекламное сообщение', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'picture',
                'filter'    => false,
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var \app\models\Advertisement $model */
                    $url = \Yii::$app->request->baseUrl.$model->picture;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '50%']);
                }
            ],
            'is_draft:boolean',
            'is_send:boolean',
            'created_at',
            'send_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
