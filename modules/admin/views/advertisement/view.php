<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Advertisement */

$this->title = 'Реклама №'.$model->id.': '.$model->name;
$this->params['breadcrumbs'][] = ['label' => 'Рекалама', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="advertisement-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить это рекламное объявление?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
        <?php if(!$model->is_draft && !$model->is_send) {
            echo Html::a('Отправить', ['send', 'id' => $model->id], ['class' => 'btn btn-warning']);
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'text:ntext',
            [
                'attribute' => 'picture',
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var \app\models\Advertisement $model */
                    $url = \Yii::$app->request->baseUrl.$model->picture;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '75%']);
                }
            ],
            'is_draft:boolean',
            'is_send:boolean',
            'created_at',
            'updated_at',
            'send_at',
            [
                'attribute' => 'sended_clients',
                'label' => 'Кому отправлено',
                'format' => 'raw',
                'value' => function($model) {
                    /** @var \app\models\AdvertisementClient[] $advertisingsClient */
                    $advertisingsClient = $model->advertisementClients;

                    if(!empty($advertisingsClient)) {
                        $string = '';

                        foreach ($advertisingsClient as $item) {

                            $string .=  Html::a($item->client->getFullName(), \yii\helpers\Url::toRoute(['client/view', 'id' => $item->client_id]));
                            $string .= "<br>";
                        }
                        return $string;
                    }

                    return null;
                }
            ]
        ],
    ]) ?>

</div>
