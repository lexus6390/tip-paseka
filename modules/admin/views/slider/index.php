<?php

use app\models\Slider;
use richardfan\sortable\SortableGridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Изображения для слайдера';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS

.ui-sortable-handle:hover {
    cursor: pointer;
}

CSS;
$this->registerCss($css);

?>
<div class="slider-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить изображение в слайдер', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <b>Изображения показаны в порядке очередности показа. Для смены очередности перетаскивайте записи.</b>
    </p>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortUrl' => Url::to(['sortItem']),

        'columns' => [
            [
                'attribute' => 'link',
                'filter'    => false,
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var Slider $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '25%']);
                }
            ],
            'is_visible:boolean',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
