<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Slider */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="slider-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'image')->widget(FileInput::class, [
        'options'       => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview'=>[
                $model->link
            ],
            'allowedFileExtensions' => ['jpg','gif','png'],
            'previewFileType'       => 'image',
            'initialPreviewAsData'  => true,
            'fileActionSettings'    => [
                'showUpload' => false,
                'showDrag'   => false,
            ]
        ],
    ])->label('Баннер, <p>Рекомендуемый размер: 1350х700 рх</p>') ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
