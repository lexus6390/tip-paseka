<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Заказ №'.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены что хотите удалить этот заказ?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'season',
            [
                'attribute' => 'client_id',
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->client->getFullName(), Url::toRoute(['client/view', 'id' => $model->client_id]));
                }
            ],
            'total_price',
            [
                'attribute' => 'products',
                'label' => 'Детали заказа',
                'format' => 'raw',
                'value' => function($model) {

                    /** @var \app\models\OrderDetail[]|null $details */
                    $details = $model->details;
                    if(is_null($details)) {
                        return null;
                    }

                    $string = '';
                    foreach ($details as $detail) {
                        $string .= Html::a($detail->product->title, Url::toRoute(['product-card/view', 'id' => $detail->product_card_id]));
                        $string .= '('.\app\models\ProductCard::getTypePricePer($detail->units).')';
                        $string .= " - <b>".$detail->count."</b>";
                        $string .= "<br>";
                    }

                    return $string;
                }
            ],
            'is_delivery:boolean',
            'delivery_address',
            'day_of_purchase',
            'is_preorder:boolean',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
