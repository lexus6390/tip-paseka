<?php

use app\models\Order;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use unclead\multipleinput\MultipleInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $clients array */
/* @var $products array */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'season')->dropDownList(Order::getAvailableSeasons()) ?>

    <?= $form->field($model, 'client_id')->widget(Select2::class, [
        'data' => $clients,
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите клиента ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]) ?>

    <?= $form->field($model, 'is_delivery')->checkbox() ?>

    <?= $form->field($model, 'delivery_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'day_of_purchase')->widget(DateTimePicker::class, [
        'language' => 'ru',
        'options' => ['placeholder' => 'Выберите дату и время покупки ...'],
        'convertFormat' => true,
        'pluginOptions' => [
            'format' => 'yyyy-M-d H:i:s',
            'todayHighlight' => true
        ]
    ]) ?>

    <?= $form->field($model, 'is_preorder')->checkbox() ?>

    <?= $form->field($model, 'products')->widget(MultipleInput::class, [
        'min' => 0,
        'max' => 15,
        'columns' => [
            [
                'name'  => 'product',
                'type'  => Select2::class,
                'title' => 'Продукт',
                'options' => ['data' => $products],
                'value' => function($data) {
                    return $data['product'];
                }
            ],
            [
                'name' => 'count',
                'type' => 'textInput',
                'title' => 'Количество',
                'value' => function($data) {
                    return $data['count'];
                }
            ]
        ]
    ])->label('Продукты в заказе') ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
