<?php

use app\models\Client;
use app\models\Order;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить заказ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'season',
                'filter' => Order::getAvailableSeasons(),
                'value' => function($model) {
                    return $model->season;
                }
            ],
            [
                'attribute' => 'client_id',
                'filter' => \kartik\select2\Select2::widget([
                    'name' => 'OrderSearch[client_id]',
                    'data' => Client::getClientList(),
                    'value' => $searchModel->client_id,
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Выберите клиента'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'selectOnClose' => true,
                    ]
                ]),
                'format' => 'raw',
                'value' => function($model) {
                    return Html::a($model->client->getFullName(), Url::toRoute(['client/view', 'id' => $model->client_id]));
                }
            ],
            [
                'attribute' => 'products',
                'label' => 'Детали заказа',
                'format' => 'raw',
                'value' => function($model) {

                    /** @var \app\models\OrderDetail[]|null $details */
                    $details = $model->details;
                    if(is_null($details)) {
                        return null;
                    }

                    $string = '';
                    foreach ($details as $detail) {
                        $string .= Html::a($detail->product->title, Url::toRoute(['product-card/view', ['id' => $detail->product_card_id]]));
                        $string .= '('.\app\models\ProductCard::getTypePricePer($detail->units).')';
                        $string .= " - <b>".$detail->count."</b>";
                        $string .= "<br>";
                    }

                    return $string;
                }
            ],
            'total_price',
            'day_of_purchase',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
