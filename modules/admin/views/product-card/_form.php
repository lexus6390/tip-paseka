<?php

use app\models\DatabaseCategory;
use app\models\ProductCard;
use kartik\file\FileInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use unclead\multipleinput\MultipleInput;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model app\models\ProductCard */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-card-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'short_description')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                'table',
                'fontcolor',
                'fontfamily',
                'fontsize',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'full_description')->widget(Widget::class, [
        'settings' => [
            'lang' => 'ru',
            'minHeight' => 200,
            'plugins' => [
                'fullscreen',
                'table',
                'fontcolor',
                'fontfamily',
                'fontsize',
            ],
        ],
    ]); ?>

    <?= $form->field($model, 'info')->widget(MultipleInput::class, [
        'min' => 0,
        'max' => 15,
        'columns' => [
            [
                'name'  => 'key',
                'type'  => 'textInput',
                'title' => 'Описание',
                'value' => function($data) {
                    return $data->key;
                }
            ],
            [
                'name'  => 'value',
                'type'  => 'textInput',
                'title' => 'Значение',
                'value' => function($data) {
                    return $data->value;
                }
            ]
        ]
    ]) ?>

    <?= $form->field($model, 'is_visible')->checkbox() ?>

    <?= $form->field($model, 'is_available')->checkbox() ?>

    <?= $form->field($model, 'is_pre_order')->checkbox() ?>

    <?= $form->field($model, 'date_available')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_pre_order')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::class, [
        'options'       => ['accept' => 'image/*'],
        'pluginOptions' => [
            'initialPreview'=>[
                $model->link
            ],
            'allowedFileExtensions' => ['jpg','gif','png'],
            'previewFileType'       => 'image',
            'initialPreviewAsData'  => true,
            'fileActionSettings'    => [
                'showUpload' => false,
                'showDrag'   => false,
            ],
            'initialPreviewConfig' =>  [
                [
                    'caption' => $model->link,
                    'url'     => Url::toRoute(['project/delete-image']),
                    'key'     => $model->id,

                ]
            ],
        ],
    ]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'price_per')->dropDownList(ProductCard::getTypesPricePer()) ?>

    <?= $form->field($model, 'database_category_id')->dropDownList(DatabaseCategory::getCategories(true)) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
