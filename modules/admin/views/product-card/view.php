<?php

use app\models\ProductCard;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ProductCard */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Product Cards', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-card-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить этот продукт?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'short_description:html',
            'full_description:html',
            [
                'attribute' => 'info',
                'value' => function($model) {
                    $info = [];
                    foreach ((array)json_decode($model->info) as $item) {
                        $info[] = implode(', ', (array)$item);
                    }

                    return implode(', ', $info);
                }
            ],
            'is_visible:boolean',
            'is_available:boolean',
            'is_pre_order:boolean',
            'date_available',
            'date_pre_order',
            [
                'attribute' => 'link',
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var ProductCard $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '75%']);
                }
            ],
            'price',
            [
                'attribute' => 'price_per',
                'value'     => function($model) {
                    /** @var ProductCard $model */
                    return ProductCard::getTypePricePer($model->price_per);
                }
            ],
            [
                'attribute' => 'database_category_id',
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var ProductCard $model */
                    return Html::a($model->category->title, Url::toRoute(['database-category/view', 'id' => $model->database_category_id]), ['target' => '_blank']);
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
