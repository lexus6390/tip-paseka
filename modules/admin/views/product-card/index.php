<?php

use app\models\ProductCard;
use richardfan\sortable\SortableGridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ProductCardSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS

.ui-sortable-handle:hover {
    cursor: pointer;
}

CSS;
$this->registerCss($css);

?>
<div class="product-card-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <b>Записи показаны в порядке очередности отображения. Для смены очередности перетаскивайте записи.</b>
    </p>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortUrl' => Url::to(['sortItem']),
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'is_visible:boolean',
            'is_available:boolean',
            [
                'attribute' => 'link',
                'filter'    => false,
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var ProductCard $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '25%']);
                }
            ],
            'price',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
