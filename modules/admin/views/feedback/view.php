<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
        <?php if(!$model->is_read) {
            echo Html::a('Пометить как прочитанное', ['read', 'id' => $model->id], ['class' => 'btn btn-primary']);
        } ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'last_name',
            'email:email',
            'theme',
            'message:ntext',
            'is_read:boolean',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
