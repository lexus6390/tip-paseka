<?php

use richardfan\sortable\SortableGridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\DatabaseCategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Категории статей';
$this->params['breadcrumbs'][] = $this->title;

$css = <<<CSS

.ui-sortable-handle:hover {
    cursor: pointer;
}

CSS;
$this->registerCss($css);
?>
<div class="database-category-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить категорию статей', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <p>
        <b>Изображения показаны в порядке очередности показа. Для смены очередности перетаскивайте записи.</b>
    </p>

    <?= SortableGridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'sortUrl' => Url::to(['sortItem']),

        'columns' => [
            'title',
            [
                'attribute' => 'link',
                'filter'    => false,
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var \app\models\DatabaseCategory $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '25%']);
                }
            ],
            [
                'attribute' => 'created_at',
                'label'     => 'Количество статей',
                'filter'    => false,
                'value'     => function($model) {
                    /** @var \app\models\DatabaseCategory $model */
                    return count($model->articles);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
