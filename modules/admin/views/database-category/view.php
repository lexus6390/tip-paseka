<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DatabaseCategory */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Database Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="database-category-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Уверены, что хотите удалить эту категорию?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Вернуться', ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'priority',
            [
                'attribute' => 'link',
                'format'    => 'raw',
                'value'     => function($model) {
                    /** @var \app\models\DatabaseCategory $model */
                    $url = \Yii::$app->request->baseUrl.$model->link;
                    return Html::img($url, ['alt'=>'myImage', 'width' => '75%']);
                }
            ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
