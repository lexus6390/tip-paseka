<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\ResetPasswordForm */
/* @var $isSend bool */
/* @var $email string */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-3"></div>
    <div class="row">
        <div class="col-lg-5">

            <?php if(isset($error)) { ?>
                <div class="alert alert-danger">
                    При смене пароля произошла ошибка. Новый пароль не сохранен
                </div>
            <?php } else { ?>
                <div class="alert alert-info">
                    <b>Для смены текущего пароля введите новый пароль дважды.</b>
                </div>
            <?php } ?>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'password')->passwordInput([
                'autofocus' => true,
                'placeholder' => 'Введите новый пароль'
            ]) ?>

            <?= $form->field($model, 'repeatPassword')->passwordInput([
                'placeholder' => 'Введите пароль еще раз'
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Сменить пароль', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>