<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\forms\RequestChangePasswordForm */
/* @var $isSend bool */
/* @var $email string */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>

<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="col-md-3"></div>
    <div class="row">
        <div class="col-lg-5">

            <?php if($isSend) { ?>
                <div class="alert alert-success">
                    На указанный email <strong>"<?= $email ?>"</strong> отправлено письмо с инструкциями. Проверьте почтовый ящик.
                </div>
            <?php } else { ?>
                <div class="alert alert-info">
                    <b>Введите свой e-mail и мы отправим Вам письмо с инструкциями</b>
                </div>
            <?php } ?>

            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'login')->textInput(['autofocus' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Отправить письмо', ['class' => 'btn btn-success', 'name' => 'login-button']) ?>
                <?= Html::a('Вернуться', Url::toRoute('default/index'), ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>