<?php

use app\models\Client;
use app\models\OrderDetail;
use dosamigos\datepicker\DatePicker;
use kartik\export\ExportMenu;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $reportFilterForm \app\models\forms\ReportFilterForm */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $issetClientId boolean */

$this->title = 'Отчет по проекту';
$this->params['breadcrumbs'][] = $this->title;

if($issetClientId) {
    $reportTitle = 'Отчет по клиенту '.$dataProvider->models[0]->order->client->getFullName().' за период с '
        .Yii::$app->formatter->asDate($reportFilterForm->dateStart, 'long')
        .' по '
        .Yii::$app->formatter->asDate($reportFilterForm->dateEnd, 'long');
}

$totalPrice = 0;
$totalCount = 0;
foreach($dataProvider->models as $orderDetail) {
    $totalPrice += $orderDetail->price_per_one_unit * $orderDetail->count;
    $totalCount += $orderDetail->count;
}

$groupedDataByDate = ArrayHelper::map(
    $dataProvider->models,
    'id',
    function ($model) {
        return $model;
    },
    'order.day_of_purchase'
);

?>

<?php $form = ActiveForm::begin(['method' => 'get']) ?>

    <div class="col-md-offset-3 col-md-3">
        <?= $form->field($reportFilterForm, 'dateStart')->widget(
            DatePicker::class, [
            // inline too, not bad
            'inline' => true,
            'language' => 'ru',
            'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-d'
            ]
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($reportFilterForm, 'dateEnd')->widget(
            DatePicker::class, [
            // inline too, not bad
            'inline' => true,
            'language' => 'ru',
            'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-d'
            ]
        ]) ?>
    </div>

    <div class="col-md-offset-3 col-md-6">
        <?= $form->field($reportFilterForm, 'client')->widget(Select2::class, [
            'data' => Client::getClientList(),
            'language' => 'ru',
            'options' => ['placeholder' => 'Выберите клиента ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]) ?>
    </div>

    <div class="col-md-offset-5 col-md-6 form-group">
        <?= Html::submitButton('Сформировать отчёт', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::begin(); ?>

    <?php if($issetClientId) { ?>
        <div class="col-md-12" id="list-result">

            <?php

            $gridColumns = [
                ['class' => 'kartik\grid\SerialColumn'],

                [
                    'attribute' => 'day_of_purchase',
                    'label' => 'Дата заказа',
                    'value' => function($model) {
                        return Yii::$app->formatter->asDate($model->order->day_of_purchase, 'long');
                    },
                    'group' => true,  // enable grouping,
                    'groupFooter' => function ($model, $key, $index, $widget) use ($groupedDataByDate) {
                        // Итого по группе "Продукт"
                        $arrCount = ArrayHelper::getColumn($groupedDataByDate[$model->order->day_of_purchase], 'count');
                        $arrPrice = ArrayHelper::getColumn($groupedDataByDate[$model->order->day_of_purchase], 'price_per_one_unit');

                        $a = array_combine($arrCount, $arrPrice);

                        $sumPrice = 0;
                        foreach ($a as $count => $pricePerOne) {
                            $sumPrice += $pricePerOne * $count;
                        }

                        return [
                            'mergeColumns' => [[1,2]], // columns to merge in summary
                            'content' => [             // content to show in each summary cell
                                1 => 'Итого за ' . Yii::$app->formatter->asDate($model->order->day_of_purchase, 'long'),
                                4 => array_sum($arrCount), //'Количество купленного продукта здесь',
                                5 => $sumPrice.'₽' //'Сумма денег, на которую купили продукта здесь'
                            ],
                            'contentOptions' => [      // content html attributes for each summary cell
                                1 => ['style' => 'font-variant:small-caps'],
                                4 => ['style' => 'text-align:right'],
                                5 => ['style' => 'text-align:right'],
                            ],
                            // html attributes for group summary row
                            'options' => ['class' => 'info table-info','style' => 'font-weight:bold;']
                        ];
                    },
                    'footer' => '<p style="font-size: 15px"><b>Итого за выбранный период</b></p>',
                ],
                [
                    'attribute' => 'product_card_id',
                    'filter' => false,
                    'label' => 'Продукт',
                    'value' => function($model) {
                        return $model->product->title;
                    },
                ],
                [
                    'attribute' => 'units',
                    'filter' => false,
                    'headerOptions' => ['style' => 'width:10%'],
                    'value' => function($model) {
                        return OrderDetail::getUnitName($model->units);
                    },
                ],
                [
                    'attribute' => 'count',
                    'filter' => false,
                    'headerOptions' => ['style' => 'width:5%'],
                    'contentOptions'=> ['style' => 'text-align:right'],
                    'value' => function($model) {
                        return $model->count;
                    },
                    'footer' => '<p style="font-size: 15px"><b>'.$totalCount.'</b></p>',
                    'footerOptions' => ['style' => 'text-align:right;']
                ],
                [
                    'attribute' => 'total_price',
                    'label' => 'Сумма',
                    'headerOptions' => ['style' => 'width:15%'],
                    'contentOptions'=> ['style' => 'text-align:right'],
                    'value' => function($model) {
                        return ($model->price_per_one_unit * $model->count).'₽';
                    },
                    'footer' => '<p style="font-size: 15px"><b>'.$totalPrice.'₽</b></p>',
                    'footerOptions' => ['style' => 'text-align:right;']
                ]

            ];

            $fullExportMenu = ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'pjaxContainerId' => 'kv-pjax-container',
                'dropdownOptions' => [
                    'label' => 'Экспорт документа',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
                'exportConfig' => [
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_CSV => false
                ]
            ]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'showPageSummary' => false,
                'showFooter' => true,
                'footerRowOptions' => [
                    'style' => 'background-color: #96cde9',
                ],
                'pjax' => true,
                'striped' => true,
                'hover' => true,
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => $reportTitle
                ],
                'toggleData' => true,
                'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                'columns' => $gridColumns,
                'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
                'toolbar' => [
                    $fullExportMenu
                ]
            ]); ?>

        </div>
    <?php } ?>

<?php Pjax::end(); ?>
