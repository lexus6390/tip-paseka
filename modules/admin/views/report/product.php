<?php

use app\models\OrderDetail;
use app\models\ProductCard;
use dosamigos\datepicker\DatePicker;
use kartik\export\ExportMenu;
use kartik\form\ActiveForm;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $reportFilterForm \app\models\forms\ReportFilterForm */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по проекту';
$this->params['breadcrumbs'][] = $this->title;
$reportTitle = 'Отчет по продуктам за период с '
    .Yii::$app->formatter->asDate($reportFilterForm->dateStart, 'long')
    .' по '
    .Yii::$app->formatter->asDate($reportFilterForm->dateEnd, 'long');

$totalPrice = 0;
$totalCount = 0;
foreach($dataProvider->models as $orderDetail) {
    $totalPrice += $orderDetail->price_per_one_unit * $orderDetail->count;
    $totalCount += $orderDetail->count;
}

$groupedDataByProduct = ArrayHelper::map(
    $dataProvider->models,
    'id',
    function ($model) {
        return $model;
    },
    'product_card_id'
);

?>

<?php $form = ActiveForm::begin(['method' => 'get']) ?>

    <div class="col-md-offset-3 col-md-3">
        <?= $form->field($reportFilterForm, 'dateStart')->widget(
            DatePicker::class, [
            // inline too, not bad
            'inline' => true,
            'language' => 'ru',
            'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-d'
            ]
        ]) ?>
    </div>

    <div class="col-md-6">
        <?= $form->field($reportFilterForm, 'dateEnd')->widget(
            DatePicker::class, [
            // inline too, not bad
            'inline' => true,
            'language' => 'ru',
            'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
            'clientOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-d'
            ]
        ]) ?>
    </div>

    <div class="col-md-offset-5 col-md-6 form-group">
        <?= Html::submitButton('Сформировать отчёт', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

<?php Pjax::begin(); ?>
    <div class="col-md-12" id="list-result">

        <?php

        $gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],

            [
                'attribute' => 'product_card_id',
                'label' => 'Продукт',
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ProductCard::getProductListForReport(),
                'value' => function($model) {
                    return $model->product->title;
                },
                'group' => true,  // enable grouping,
                'groupFooter' => function ($model, $key, $index, $widget) use ($groupedDataByProduct) {

                    // Итого по группе "Продукт"
                    $arrCount = ArrayHelper::getColumn($groupedDataByProduct[$model->product_card_id], 'count');
                    $arrPrice = ArrayHelper::getColumn($groupedDataByProduct[$model->product_card_id], 'price_per_one_unit');

                    $a = [];
                    foreach ($arrPrice as $keyPrice => $valuePrice) {
                        $a[$keyPrice] = [$valuePrice, $arrCount[$keyPrice]];
                    }

                    $sumPrice = 0;
                    foreach ($a as $key => $arrData) {
                        $sumPrice += $arrData[0] * $arrData[1];
                    }

                    return [
                        'mergeColumns' => [[1,2]], // columns to merge in summary
                        'content' => [             // content to show in each summary cell
                            1 => 'Итого по продукту "' . $model->product->title.'"',
                            5 => array_sum($arrCount), //'Количество купленного продукта здесь',
                            6 => $sumPrice.'₽' //'Сумма денег, на которую купили продукта здесь'
                        ],
                        'contentOptions' => [      // content html attributes for each summary cell
                            1 => ['style' => 'font-variant:small-caps'],
                            5 => ['style' => 'text-align:right'],
                            6 => ['style' => 'text-align:right'],
                        ],
                        // html attributes for group summary row
                        'options' => ['class' => 'info table-info','style' => 'font-weight:bold;']
                    ];
                },
                'footer' => '<p style="font-size: 15px"><b>Итого</b></p>',
            ],
            [
                'attribute' => 'units',
                'filter' => false,
                'headerOptions' => ['style' => 'width:10%'],
                'contentOptions'=> ['style' => 'text-align:center'],
                'value' => function($model) {
                    return OrderDetail::getUnitName($model->units);
                },
                'group' => true,  // enable grouping,
                'subGroupOf' => 1
            ],
            [
                'attribute' => 'order_id',
                'label' => 'Заказчик',
                'filter' => false,
                'headerOptions' => ['style' => 'width:25%'],
                'contentOptions'=> ['style' => 'text-align:center'],
                'value' => function($model) {
                    return $model->order->client->getFullName();
                },
            ],
            [
                'attribute' => 'day_of_purchase',
                'headerOptions' => ['style' => 'width:15%'],
                'contentOptions'=> ['style' => 'text-align:center'],
                'label' => 'Дата заказа',
                'value' => function($model) {
                    return Yii::$app->formatter->asDate($model->order->day_of_purchase, 'long');
                }
            ],
            [
                'attribute' => 'count',
                'filter' => false,
                'headerOptions' => ['style' => 'width:5%'],
                'contentOptions'=> ['style' => 'text-align:right'],
                'value' => function($model) {
                    return $model->count;
                },
                'footer' => '<p style="font-size: 15px"><b>'.$totalCount.'</b></p>',
                'footerOptions' => ['style' => 'text-align:right;']
            ],
            [
                'attribute' => 'total_price',
                'label' => 'Сумма',
                'headerOptions' => ['style' => 'width:10%'],
                'contentOptions'=> ['style' => 'text-align:right'],
                'value' => function($model) {
                    return ($model->price_per_one_unit * $model->count).'₽';
                },
                'footer' => '<p style="font-size: 15px"><b>'.$totalPrice.'₽</b></p>',
                'footerOptions' => ['style' => 'text-align:right;']
            ]

        ];

        $fullExportMenu = ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'fontAwesome' => true,
            'pjaxContainerId' => 'kv-pjax-container',
            'dropdownOptions' => [
                'label' => 'Экспорт документа',
                'class' => 'btn btn-default',
                'itemsBefore' => [
                    '<li class="dropdown-header">Export All Data</li>',
                ],
            ],
            'exportConfig' => [
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_CSV => false
            ]
        ]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'showPageSummary' => false,
            'showFooter' => true,
            'footerRowOptions' => [
                'style' => 'background-color: #96cde9',
            ],
            'pjax' => true,
            'striped' => true,
            'hover' => true,
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
                'heading' => $reportTitle
            ],
            'toggleData' => true,
            'toggleDataContainer' => ['class' => 'btn-group mr-2'],
            'columns' => $gridColumns,
            'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
            'toolbar' => [
                $fullExportMenu
            ]
        ]); ?>

    </div>
<?php Pjax::end(); ?>

