<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var $this yii\web\View */
/** @var $dataProvider yii\data\ActiveDataProvider */
/** @var $model \app\models\MainPageInfo */

$this->title = 'Инфо главной страницы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="main-page-info-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'phone',
            'email',
            'main_title',
            'main_text:ntext',
            'is_visible_main_text:boolean',
            'is_visible_our_products:boolean',
            'is_visible_review:boolean',
            'is_visible_database:boolean',
            'is_visible_subscribe:boolean',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
