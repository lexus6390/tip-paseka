<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MainPageInfo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="main-page-info-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'main_text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'is_visible_main_text')->checkbox() ?>

    <?= $form->field($model, 'is_visible_our_products')->checkbox() ?>

    <?= $form->field($model, 'is_visible_review')->checkbox() ?>

    <?= $form->field($model, 'is_visible_database')->checkbox() ?>

    <?= $form->field($model, 'is_visible_subscribe')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
