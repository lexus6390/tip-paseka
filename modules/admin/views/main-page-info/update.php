<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MainPageInfo */

$this->title = 'Редактировать инфо главной страницы';
$this->params['breadcrumbs'][] = ['label' => 'Инфо главной страницы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="main-page-info-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
