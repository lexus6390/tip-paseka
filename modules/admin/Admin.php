<?php

namespace app\modules\admin;

use yii\base\Module;
use yii\helpers\ArrayHelper;

/**
 * admin module definition class
 */
class Admin extends Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        
        $params = \Yii::$app->params;

        \Yii::configure(\Yii::$app, require(__DIR__ . '/config/main.php'));
        
        \Yii::$app->params = ArrayHelper::merge($params, \Yii::$app->params);

        \Yii::$app->errorHandler->errorAction = 'admin/default/error';
    }
}
