var params = window
    .location
    .search
    .replace('?','')
    .split('&')
    .reduce(
        function(p,e){
            var a = e.split('=');
            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
        {}
    );

// Показ модального окна об успешной отправке сообщения
if(params['is_send'] == 1) {
    $('#overlay_success').fadeIn(1400, function() {
        $('#modal_success').css('display', 'block').animate({opacity: 1, top: '50%'}, 200);
    });
}

// Закрытие модального окна
$(document).on('click', '#close_modal', function(e) {
    $('#overlay_success').fadeOut(1000);
    $('#modal_success').css('display', 'none').animate({opacity: 1, top: '50%'}, 200);
});

$(document).on('click', '#overlay_success', function(e) {
    $('#overlay_success').fadeOut(1000);
    $('#modal_success').css('display', 'none').animate({opacity: 1, top: '50%'}, 200);
});

// Отправка формы
$(document).on('click', '#send', function(e) {

    e.preventDefault();

    $.ajax({
        url: '/contact',
        method: 'POST',
        data: {
            name: $(document).find('#fname').val(),
            last_name: $(document).find('#lname').val(),
            email: $(document).find('#email').val(),
            theme: $(document).find('#subject').val(),
            message: $(document).find('#message').val(),
            _csrf : $('meta[name="csrf-token"]').attr("content")
        },
        success: function(response) {

            if(response.errors != undefined) {
                $(document).find('.errors').html(response.errors);
            }

            if(response.success) {
                window.location.replace(response.link);
            }
        }
    });
});