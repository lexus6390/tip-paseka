<?php

namespace app\services\Client;

use app\models\Client;

/**
 * Class ClientService
 * @package app\services\Client
 */
class ClientService
{
    /**
     * Поиск клиента по переданному номеру телефона
     * Если найден, то добавляем клиенту ViberID и ViberName
     *
     * @param string $phoneNumber
     * @param string $viberId
     * @param string $viberName
     * @return bool
     */
    public function subscribeClientOnViber(string $phoneNumber, string $viberId, string $viberName)
    {
        $phoneNumber = $this->getPhoneNumberInFormat($phoneNumber);

        $client = Client::findOne(['phone' => $phoneNumber]);

        if(is_null($client)) {
            return false;
        }

        $client->setAttributes([
            'viber_id'   => $viberId,
            'viber_name' => $viberName
        ]);

        if(!$client->save()) {
            return false;
        }

        return true;
    }

    /**
     * Получение телефонного номера в нужном формате для поиска в базе данных
     * Получает на вход номер формата: '79876543210'
     * Возвращает номер в формате: '+7 (987) 654-32-10'
     *
     * @param $phoneNumber string
     * @return string
     */
    private function getPhoneNumberInFormat($phoneNumber)
    {
        $asArr = str_split($phoneNumber);

        return '+'.$asArr[0].' '.'('.$asArr[1].$asArr[2].$asArr[3].') '.$asArr[4].$asArr[5].$asArr[6].'-'.$asArr[7].$asArr[8].'-'.$asArr[9].$asArr[10];
    }
}