<?php

namespace app\services\MailNotifications;

/**
 * Класс отправки письма при получении нового сообщения обратной связи
 */
class FeedbackNotification implements INotify
{
    /**
     * Файл представления для письма
     * @var string
     */
    private $view = 'feedback-html';

    /**
     * Тема письма
     * @var string
     */
    private $subject = 'Новое сообщение обратной связи';

    /**
     * @param $message
     * @param null $address
     * @return bool
     */
    public function send($message, $address = null)
    {
        $notifier = \Yii::$app->mailer;
        $notifier = $notifier->compose($this->view, [
            'feedback' => $message
        ])
            ->setFrom(\Yii::$app->params['mail_from'])
            ->setTo(\Yii::$app->params['mail_to'])
            ->setSubject($this->subject);

        return $notifier->send() ? true : false;
    }
}