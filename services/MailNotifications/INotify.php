<?php

namespace app\services\MailNotifications;

/**
 * Интерфейс для реализации метода отправки email-сообщения
 */
interface INotify
{
    /**
     * @param $message
     * @param null $address
     * @return mixed
     */
    public function send($message, $address = null);
}