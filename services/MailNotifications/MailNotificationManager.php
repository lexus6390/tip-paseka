<?php

namespace app\services\MailNotifications;

/**
 * Менеджер отправки email
 */
class MailNotificationManager
{
    /**
     * Название типа email-уведомления для инициализации нужного класса
     * @var string
     */
    public $typeEmailClass;

    /**
     * Сообщение с необходимыми данными для подстановки в шаблон письма
     * @var string|array|object
     */
    public $message;

    /**
     * E-mail адрес получателя письма
     * @var string
     */
    public $address;

    /**
     * Пространство имен для классов отправки сообщений
     * @var string
     */
    private $namespaceClasses = 'app\services\MailNotifications\\';

    /**
     * MailNotificationManager constructor.
     * @param $typeEmailClass
     * @param $message
     * @param $address
     */
    public function __construct($typeEmailClass, $message, $address)
    {
        $this->typeEmailClass = \Yii::createObject($this->namespaceClasses.$typeEmailClass);
        $this->message = $message;
        $this->address = $address;
    }

    /**
     * Отправка сообщения указанного типа
     */
    public function sendEmail()
    {
        return $this->typeEmailClass->send($this->message, $this->address);
    }
}