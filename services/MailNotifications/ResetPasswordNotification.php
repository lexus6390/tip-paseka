<?php

namespace app\services\MailNotifications;

/**
 * Класс отправки письма для смены пароля администратора
 */
class ResetPasswordNotification implements INotify
{
    /**
     * Файл представления для письма
     * @var string
     */
    private $view = 'passwordResetToken-html';

    /**
     * Тема письма
     * @var string
     */
    private $subject = 'Смена пароля';

    /**
     * @param $message
     * @param null $address
     * @return bool
     */
    public function send($message, $address = null)
    {
        $notifier = \Yii::$app->mailer;
        $notifier = $notifier->compose($this->view, [
            'user' => $message
        ])
            ->setFrom(\Yii::$app->params['mail_from'])
            ->setTo($address)
            ->setSubject($this->subject);

        return $notifier->send() ? true : false;
    }
}