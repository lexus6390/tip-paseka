<?php

namespace app\services\ViberBot;

use app\models\Advertisement;
use app\models\AdvertisementClient;
use app\models\Client;
use app\services\Client\ClientService;
use Viber\Bot;
use Viber\Api\Sender;

/**
 * Class ViberBot
 * @package app\services\ViberBot
 */
class ViberBot
{
    /**
     * Установка бота
     */
    public function setViberBot()
    {
        $botSender = $this->getSender();

        try {
            $bot = new Bot(['token' => \Yii::$app->params['viber_api_key']]);
            $bot->onConversation(function ($event) use ($bot, $botSender) {
                    // Пользователь вошел в чат
                    // Разрешается написать только одно сообщение
                    return (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setText('Добро пожаловать! Чтобы начать получать от меня выгодные предложения напишите мне "Начать"');
                })
                ->onText('|Начать|si', function ($event) use ($bot, $botSender) {
                    $receiverId = $event->getSender()->getId();

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($receiverId)
                            ->setMinApiVersion(3)
                            ->setText("Нам необходим Ваш номер телефона")
                            ->setKeyboard(
                                (new \Viber\Api\Keyboard())
                                    ->setButtons([
                                        (new \Viber\Api\Keyboard\Button())
                                            ->setActionType('share-phone')
                                            ->setActionBody('reply')
                                            ->setText('Отправить номер телефона')
                                    ])
                            )
                    );
                })
                ->on(function ($event) {
                    return ($event instanceof \Viber\Api\Event\Message
                        && $event->getMessage() instanceof \Viber\Api\Message\Contact);
                }, function ($event) use ($bot, $botSender) {
                    $phoneNumber = $event->getMessage()->getPhoneNumber();
                    $receiverId = $event->getSender()->getId();
                    $receiverName = $event->getSender()->getName();

                    // Добавление подписчика
                    $clientService = new ClientService();
                    $clientService->subscribeClientOnViber($phoneNumber, $receiverId, $receiverName);

                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($receiverId)
                            ->setText('Спасибо, номер получен! В будущем я буду уведомлять Вас о самых выгодных и актуальных предложениях.')
                    );
                })
                ->run();
        } catch (\Exception $e) {
            // log exceptions
            $logger = \Yii::$app->monolog->getLogger();
            $logger->log('info', $e->getMessage());
        }
    }

    /**
     * Отправка рекламного сообщения подписчикам бота
     *
     * @param Advertisement $advertisement
     * @return bool
     */
    public function sendMessage(Advertisement $advertisement)
    {
        /** @var Client[] $receivers */
        $receivers = Client::find()
            ->where(['has_viber' => 1])
            ->andWhere(['is not', 'viber_id', null])
            ->andWhere(['is not', 'viber_name', null])
            ->all();

        if(empty($receivers)) {
            return false;
        }

        $botSender = $this->getSender();

        $bot = new Bot(['token' => \Yii::$app->params['viber_api_key']]);

        foreach ($receivers as $receiver) {

            $advertisementText = 'Доброго дня, '.$receiver->viber_name.'! '.$advertisement->text;

            // Если в рекламе есть картинка, то тип сообщения - "Picture"
            if(!is_null($advertisement->picture)) {
                try {
                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Picture())
                            ->setSender($botSender)
                            ->setReceiver($receiver->viber_id)
                            ->setMedia(\Yii::$app->params['domain'].$advertisement->picture)
                            ->setText($advertisementText)
                    );

                    $advertisementClient = new AdvertisementClient();
                    $advertisementClient->setAttributes([
                        'client_id'        => $receiver->id,
                        'advertisement_id' => $advertisement->id
                    ]);
                    $advertisementClient->save();
                } catch (\Exception $e) {

                }

            } else {
                // Иначе тип сообщения - "Text"
                try {
                    $bot->getClient()->sendMessage(
                        (new \Viber\Api\Message\Text())
                            ->setSender($botSender)
                            ->setReceiver($receiver->viber_id)
                            ->setText($advertisementText)
                    );

                    $advertisementClient = new AdvertisementClient();
                    $advertisementClient->setAttributes([
                        'client_id'        => $receiver->id,
                        'advertisement_id' => $advertisement->id
                    ]);
                    $advertisementClient->save();
                } catch (\Exception $e) {

                }
            }
        }
        return true;
    }

    /**
     * Получение сущности отправителя
     * @return Sender
     */
    private function getSender() : Sender
    {
        return new Sender([
            'name'   => 'Пасечник',
            'avatar' => 'https://tip-paseka.ru/images/logo.png',
        ]);
    }
}