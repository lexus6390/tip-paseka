<?php

namespace app\services\ViberBot;

use http\Exception;
use Viber\Client;

/**
 * Class ViberSetWebhook
 * @package app\services\ViberBot
 */
class ViberSetWebhook
{
    /**
     * URL для работы бота
     */
    const WEBHOOK_URL = 'https://tip-paseka.ru/bot/viber';

    /**
     * Установка вебхука для работы бота
     */
    public function setWebHook() : void
    {
        try{
            $client = new Client(['token' => \Yii::$app->params['viber_api_key']]);
            $client->setWebhook(self::WEBHOOK_URL);
            echo 'Success';
        } catch (Exception $e) {
            echo 'Fail, see logs';
        }
    }
}