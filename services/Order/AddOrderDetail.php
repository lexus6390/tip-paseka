<?php

namespace app\services\Order;

use app\models\Order;
use app\models\OrderDetail;
use app\models\ProductCard;
use yii\web\ServerErrorHttpException;

/**
 * Class AddOrderDetail
 * @package app\services\Order
 */
class AddOrderDetail
{
    /**
     * Общая сумма заказа
     * @var int
     */
    public $totalPrice;

    /**
     * @param Order $order
     * @return bool
     * @throws ServerErrorHttpException
     */
    public function addOrderDetail(Order $order)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        $this->totalPrice = 0;

        foreach ($order->products as $item) {

            /** @var ProductCard $product */
            $product = ProductCard::find()
                ->where(['id' => $item['product']])
                ->one();

            $orderDetail = new OrderDetail();
            $orderDetail->setAttributes([
                'order_id'           => $order->id,
                'product_card_id'    => $product->id,
                'units'              => $product->price_per,
                'count'              => $item['count'],
                'price_per_one_unit' => $product->price
            ]);

            if(!$orderDetail->save()) {
                $transaction->rollBack();
                throw new ServerErrorHttpException('Ошибка сохранения деталей заказа');
            }
            $this->totalPrice += $orderDetail->price_per_one_unit * $orderDetail->count;
        }

        $transaction->commit();
        return true;
    }
}