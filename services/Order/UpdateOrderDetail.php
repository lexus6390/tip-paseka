<?php

namespace app\services\Order;

use app\models\Order;
use app\models\OrderDetail;
use app\models\ProductCard;
use yii\web\ServerErrorHttpException;

/**
 * Class UpdateOrderDetail
 * @package app\services\Order
 */
class UpdateOrderDetail
{
    /**
     * Идентификаторы сущностей OrderDetail, которые были обработаны в текущем запросе
     * Все идентификаторы OrderDetail текущего заказа, которые не были обработаны (не вошли в этот масссив)
     * будут удалены из базы
     *
     * @var array
     */
    private $processedDetails = [];

    /**
     * Обновление информации о деталях заказа
     *
     * @param Order $order
     * @return bool
     * @throws ServerErrorHttpException
     */
    public function updateOrderDetail(Order $order)
    {
        $transaction = \Yii::$app->db->beginTransaction();

        foreach ($order->products as $item) {

            // Проверяем есть ли уже такой продукт в таблице OrderDetail
            /** @var OrderDetail $orderDetail */
            $orderDetail = OrderDetail::find()
                ->where([
                    'order_id'        => $order->id,
                    'product_card_id' => $item['product']
                ])->one();

            // Если есть проверяем не изменилось ли значение.
            if($orderDetail) {

                // Если не изменилось, то пропускаем, ничего не делаем
                if($orderDetail->count == $item['count']) {
                    $this->processedDetails[] = $orderDetail->id;
                    continue;
                } else {
                    // Если изменилось, то меняем на новое значение
                    $orderDetail->count = $item['count'];
                    if(!$orderDetail->save()) {
                        $transaction->rollBack();
                        throw new ServerErrorHttpException('Ошибка сохранения деталей заказа');
                    }
                    $this->processedDetails[] = $orderDetail->id;
                }

            } else {
                // Если такого продукта нет, то добавляем его
                /** @var ProductCard $product */
                $product = ProductCard::find()
                    ->where(['id' => $item['product']])
                    ->one();

                $orderDetail = new OrderDetail();
                $orderDetail->setAttributes([
                    'order_id'           => $order->id,
                    'product_card_id'    => $product->id,
                    'units'              => $product->price_per,
                    'count'              => $item['count'],
                    'price_per_one_unit' => $product->price
                ]);

                if(!$orderDetail->save()) {
                    $transaction->rollBack();
                    throw new ServerErrorHttpException('Ошибка сохранения деталей заказа');
                }
                $this->processedDetails[] = $orderDetail->id;
            }
        }

        // Удаляем записи, которые не пришли в итоговом массиве.
        $deleteOrderDetail = OrderDetail::find()
            ->where(['order_id' => $order->id])
            ->andWhere(['not in', 'id', $this->processedDetails])
            ->all();

        foreach ($deleteOrderDetail as $key => $orderDetailItem) {
            $orderDetailItem->delete();
        }

        $transaction->commit();
        return true;
    }
}