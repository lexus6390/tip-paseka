<?php

namespace app\models\search;

use yii\data\ActiveDataProvider;
use app\models\Advertisement;

/**
 * AdvertisementSearch represents the model behind the search form of `app\models\Advertisement`.
 */
class AdvertisementSearch extends Advertisement
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_draft', 'is_send'], 'integer'],
            [['text', 'picture', 'created_at', 'updated_at', 'send_at', 'name'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Advertisement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_draft' => $this->is_draft,
            'is_send' => $this->is_send,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'send_at'    => $this->send_at
        ]);

        $query->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'picture', $this->picture]);

        return $dataProvider;
    }
}
