<?php

namespace app\models\search;

use yii\data\ActiveDataProvider;
use app\models\ProductCard;

/**
 * ProductCardSearch represents the model behind the search form of `app\models\ProductCard`.
 */
class ProductCardSearch extends ProductCard
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'is_visible', 'is_available', 'is_pre_order', 'price', 'priority'], 'integer'],
            [['title', 'short_description', 'full_description', 'info', 'date_available', 'date_pre_order', 'link', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductCard::find()->orderBy('priority ASC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'is_visible' => $this->is_visible,
            'is_available' => $this->is_available,
            'is_pre_order' => $this->is_pre_order,
            'price' => $this->price,
            'priority' => $this->priority,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'full_description', $this->full_description])
            ->andFilterWhere(['like', 'info', $this->info])
            ->andFilterWhere(['like', 'date_available', $this->date_available])
            ->andFilterWhere(['like', 'date_pre_order', $this->date_pre_order])
            ->andFilterWhere(['like', 'link', $this->link]);

        return $dataProvider;
    }
}
