<?php

namespace app\models\search;

use yii\data\ActiveDataProvider;
use app\models\OrderDetail;

/**
 * OrderDetailSearch represents the model behind the search form of `app\models\OrderDetail`.
 */
class OrderDetailSearch extends OrderDetail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'order_id', 'units', 'product_card_id', 'count', 'price_per_one_unit'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrderDetail::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'product_card_id' => $this->product_card_id,
            'units' => $this->units,
            'count' => $this->count,
            'price_per_one_unit' => $this->price_per_one_unit,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }

    /**
     * Поиск по задачам во вкладке "Отчет по продуктам"
     *
     * @param array $params
     * @param string $dateStart
     * @param string $dateEnd
     *
     * @return ActiveDataProvider
     */
    public function searchForReport($params, $dateStart, $dateEnd)
    {
        $query = OrderDetail::find()->joinWith(['product'])->orderBy('product_card_id ASC');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if($this->product_card_id == 0) {
            $this->product_card_id = '';
        }

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'product_card_id' => $this->product_card_id,
            'units' => $this->units,
            'count' => $this->count,
            'price_per_one_unit' => $this->price_per_one_unit,
            'order_detail.created_at' => $this->created_at,
            'order_detail.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['>=', 'order_detail.created_at', $dateStart.' 00:00:00']);
        $query->andFilterWhere(['<=', 'order_detail.created_at', $dateEnd.' 23:59:59']);

        return $dataProvider;
    }

    /**
     * Поиск по задачам во вкладке "Отчет по продуктам"
     *
     * @param array $params
     * @param string $clientId
     * @param string $dateStart
     * @param string $dateEnd
     *
     * @return ActiveDataProvider
     */
    public function searchForClientReport($params, $clientId, $dateStart, $dateEnd)
    {
        $query = OrderDetail::find()
            ->where(['order.client_id' => $clientId])
            ->joinWith(['product', 'order'])
            ->orderBy('order.day_of_purchase ASC');

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'order_id' => $this->order_id,
            'product_card_id' => $this->product_card_id,
            'units' => $this->units,
            'count' => $this->count,
            'price_per_one_unit' => $this->price_per_one_unit,
            'order_detail.created_at' => $this->created_at,
            'order_detail.updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['>=', 'order_detail.created_at', $dateStart.' 00:00:00']);
        $query->andFilterWhere(['<=', 'order_detail.created_at', $dateEnd.' 23:59:59']);

        return $dataProvider;
    }
}
