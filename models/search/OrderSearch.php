<?php

namespace app\models\search;

use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form of `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'season', 'client_id', 'total_price', 'is_delivery', 'is_preorder'], 'integer'],
            [['delivery_address', 'day_of_purchase', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'              => $this->id,
            'season'          => $this->season,
            'client_id'       => $this->client_id,
            'total_price'     => $this->total_price,
            'is_delivery'     => $this->is_delivery,
            'day_of_purchase' => $this->day_of_purchase,
            'is_preorder'     => $this->is_preorder,
            'created_at'      => $this->created_at,
            'updated_at'      => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'delivery_address', $this->delivery_address]);

        return $dataProvider;
    }
}
