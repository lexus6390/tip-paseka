<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "advertisement".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $picture
 * @property int $is_draft
 * @property int $is_send
 * @property string $created_at
 * @property string $updated_at
 * @property string $send_at
 *
 * @property AdvertisementClient[] $advertisementClients
 */
class Advertisement extends ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['text'], 'required'],
            [['text'], 'string'],
            [['is_draft', 'is_send'], 'integer'],
            [['created_at', 'updated_at', 'send_at'], 'safe'],
            [['picture', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Название',
            'text'       => 'Текст сообщения',
            'picture'    => 'Изображение',
            'is_draft'   => 'Черновик?',
            'is_send'    => 'Отправлено?',
            'image'      => 'Изображение',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
            'send_at'    => 'Дата отправки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisementClients()
    {
        return $this->hasMany(AdvertisementClient::class, ['advertisement_id' => 'id']);
    }
}
