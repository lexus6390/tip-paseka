<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $name
 * @property string $last_name
 * @property string $email
 * @property string $theme
 * @property string $message
 * @property int $is_read
 * @property string $created_at
 * @property string $updated_at
 */
class Feedback extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'message'], 'required'],
            [['message'], 'string'],
            ['email', 'email'],
            [['is_read'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'last_name', 'email', 'theme'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => 'Имя',
            'last_name'  => 'Фамилия',
            'email'      => 'Email',
            'theme'      => 'Тема обращения',
            'message'    => 'Сообщение',
            'is_read'    => 'Прочитано',
            'created_at' => 'Дата и время создания записи',
            'updated_at' => 'Дата и время изменения записи',
        ];
    }
}
