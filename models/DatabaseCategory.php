<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

/**
 * This is the model class for table "database_category".
 *
 * @property int $id
 * @property string $title
 * @property string $link
 * @property integer $priority
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Article $articles
 */
class DatabaseCategory extends ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'database_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['priority'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'link', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'title'      => 'Название категории',
            'link'       => 'Изображение',
            'image'      => 'Изображение',
            'priority'   => 'Приоритет показа',
            'created_at' => 'Дата и время создания записи',
            'updated_at' => 'Дата и время редактирования записи',
        ];
    }

    /**
     * @param $available bool
     * @return array
     * @throws NotFoundHttpException
     */
    public static function getCategories($available = false)
    {
        /** @var DatabaseCategory[] $categories */
        $categories = self::find()->all();

        if(empty($categories)) {
            throw new NotFoundHttpException('Не найдено ни одной категории статей');
        }

        $resultCategories = ['Нет категории'];
        foreach ($categories as $category) {
            if ($available) {
                // Не выводим категории, в которых нет ни одной статьи
                if(!empty($category->articles)) {
                    $resultCategories[$category->id] = $category->title;
                }
            } else {
                $resultCategories[$category->id] = $category->title;
            }
        }

        return $resultCategories;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::class, ['category_id' => 'id']);
    }
}
