<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property int $category_id
 * @property string $title
 * @property string $description
 * @property string $author
 * @property string $text
 * @property string $link
 * @property int $is_published
 * @property string $published_date
 * @property int $priority
 * @property string $created_at
 * @property string $updated_at
 */
class Article extends ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_published', 'priority'], 'integer'],
            [['priority', 'category_id'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['text', 'description'], 'string'],
            [['title', 'author', 'link', 'published_date', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'category_id'    => 'Категория',
            'title'          => 'Заголовок',
            'description'    => 'Короткое описание статьи',
            'author'         => 'Автор статьи',
            'text'           => 'Текст статьи',
            'link'           => 'Изображение',
            'image'          => 'Изображение',
            'is_published'   => 'Опубликовано',
            'published_date' => 'Дата публикации',
            'priority'       => 'Приоритет показа',
            'created_at'     => 'Дата и время создания статьи',
            'updated_at'     => 'Дата и время редактирования статьи'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DatabaseCategory::class, ['id' => 'category_id']);
    }
}
