<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "menu_item".
 *
 * @property int $id
 * @property string $item_name
 * @property string $item_link
 * @property integer $is_visible
 * @property string $created_at
 * @property string $updated_at
 */
class MenuItem extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'menu_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_name', 'item_link'], 'required'],
            [['is_visible'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['item_name', 'item_link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'item_name'  => 'Название пункта меню',
            'item_link'  => 'Ссылка пункта меню',
            'is_visible' => 'Отображать',
            'created_at' => 'Дата создания записи',
            'updated_at' => 'Дата редактирования записи',
        ];
    }
}
