<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "product_card".
 *
 * @property int $id
 * @property string $title
 * @property string $short_description
 * @property string $full_description
 * @property string $info
 * @property int $is_visible
 * @property int $is_available
 * @property int $is_pre_order
 * @property string $date_available
 * @property string $date_pre_order
 * @property string $link
 * @property int $price
 * @property int $priority
 * @property string $created_at
 * @property string $updated_at
 * @property integer $price_per
 * @property integer $database_category_id
 *
 * @property DatabaseCategory $category
 */
class ProductCard extends ActiveRecord
{
    /**
     * Стоимость за 1 литр
     */
    const PRICE_PER_LITER = 1;

    /**
     * Стоимость за 1 килограмм
     */
    const PRICE_PER_KILOGRAM = 2;

    /**
     * Стоимость за 100 грамм
     */
    const PRICE_PER_100_GRAM = 3;

    /**
     * Стоимость за 10 грамм
     */
    const PRICE_PER_10_GRAM = 4;

    /**
     * Стоимость за 1 штуку
     */
    const PRICE_PER_ONE_PIECE = 5;

    /**
     * Стоимость за 100 миллилитров
     */
    const PRICE_PER_100_MILLILITERS = 6;

    /**
     * @var UploadedFile
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short_description', 'full_description', 'info', 'image'], 'string'],
            [
                [
                    'is_visible',
                    'is_available',
                    'is_pre_order',
                    'price',
                    'priority',
                    'price_per',
                    'database_category_id'
                ],
                'integer'
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'date_available', 'date_pre_order', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                   => 'ID',
            'title'                => 'Название продукта',
            'short_description'    => 'Короткое описание',
            'full_description'     => 'Полное описание',
            'info'                 => 'Инфо о сборе',
            'is_visible'           => 'Отображать',
            'is_available'         => 'Доступен для продажи',
            'is_pre_order'         => 'Доступен предзаказ',
            'date_available'       => 'Дата, когда будет доступен для продажи',
            'date_pre_order'       => 'Дата, когда будет доступен предзаказ',
            'link'                 => 'Изображение',
            'image'                => 'Изображение',
            'price'                => 'Цена (рублей)',
            'priority'             => 'Приоритет',
            'created_at'           => 'Дата и время создания',
            'updated_at'           => 'Дата и время редактирования',
            'price_per'            => 'Цена за...',
            'database_category_id' => 'Связанная категория статей'
        ];
    }

    /**
     * Получение вариантов "Цена за.."
     *
     * @return array
     */
    public static function getTypesPricePer()
    {
        return [
            self::PRICE_PER_LITER           => '1 литр',
            self::PRICE_PER_KILOGRAM        => '1 килограмм',
            self::PRICE_PER_100_GRAM        => '100 грамм',
            self::PRICE_PER_10_GRAM         => '10 грамм',
            self::PRICE_PER_ONE_PIECE       => 'шт.',
            self::PRICE_PER_100_MILLILITERS => '100 мл'
        ];
    }

    /**
     * Получение расшифровки типа "Цена за.." для выпадающего списка
     *
     * @param $type int
     * @return array
     */
    public static function getTypePricePer($type)
    {
        $types = self::getTypesPricePer();

        return $types[$type];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(DatabaseCategory::class, ['id' => 'database_category_id']);
    }

    /**
     * Получение списка продуктов для выпадающего списка
     * @return array
     */
    public static function getProductList()
    {
        /** @var ProductCard[] $productCards */
        $productCards = ProductCard::find()->all();
        $productList = [];
        foreach ($productCards as $productCard) {
            $productList[$productCard->id] = $productCard->title.' ('.self::getTypePricePer($productCard->price_per).')';
        }

        return $productList;
    }

    /**
     * @return array
     */
    public static function getProductListForReport()
    {
        /** @var ProductCard[] $productCards */
        $productCards = ProductCard::find()
            ->orderBy('id ASC')
            ->asArray()
            ->all();

        $productList = ArrayHelper::map($productCards, 'id', 'title');
        array_unshift($productList, 'Все');

        return $productList;
    }
}
