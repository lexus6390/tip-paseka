<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "advertisement_client".
 *
 * @property int $id
 * @property int $client_id
 * @property int $advertisement_id
 *
 * @property Client $client
 * @property Advertisement $advertisement
 */
class AdvertisementClient extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'advertisement_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'advertisement_id'], 'required'],
            [['client_id', 'advertisement_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'client_id'        => 'Клиент',
            'advertisement_id' => 'Рекламное объявление',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisement()
    {
        return $this->hasOne(Advertisement::class, ['id' => 'advertisement_id']);
    }
}
