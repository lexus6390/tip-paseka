<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "main_page_info".
 *
 * @property int $id
 * @property string $phone
 * @property string $email
 * @property string $main_title
 * @property string $main_text
 * @property int $is_visible_main_text
 * @property int $is_visible_our_products
 * @property int $is_visible_review
 * @property int $is_visible_database
 * @property int $is_visible_subscribe
 * @property string $created_at
 * @property string $updated_at
 */
class MainPageInfo extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'main_page_info';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_text'], 'string'],
            [
                [
                    'is_visible_main_text',
                    'is_visible_our_products',
                    'is_visible_review',
                    'is_visible_database',
                    'is_visible_subscribe'
                ],
                'integer'
            ],
            [['created_at', 'updated_at'], 'safe'],
            [['phone', 'main_title', 'email'], 'string', 'max' => 255],
            [['email'], 'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                      => 'ID',
            'phone'                   => 'Номер телефона для связи',
            'email'                   => 'Email для связи',
            'main_title'              => 'Приветственный заголовок',
            'main_text'               => 'Приветственный текст',
            'is_visible_main_text'    => 'Показывать приветственный текст',
            'is_visible_our_products' => 'Показывать блок "Наши продукты"',
            'is_visible_review'       => 'Показывать блок "Отзывы"',
            'is_visible_database'     => 'Показывать блок "База знаний"',
            'is_visible_subscribe'    => 'Показывать блок "Подписаться"',
            'created_at'              => 'Дата создания записи',
            'updated_at'              => 'Дата редактирования записи',
        ];
    }
}
