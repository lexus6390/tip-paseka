<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "order_detail".
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_card_id
 * @property int $units
 * @property int $count
 * @property int $price_per_one_unit
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ProductCard $product
 * @property Order $order
 */
class OrderDetail extends ActiveRecord
{
    /**
     * Единица измерения - 1 литр
     */
    const UNIT_LITER = 1;

    /**
     * Единица измерения - 1 килограмм
     */
    const UNIT_KILOGRAM = 2;

    /**
     * Единица измерения - 100 грамм
     */
    const UNIT_100_GRAM = 3;

    /**
     * Единица измерения - 10 грамм
     */
    const UNIT_10_GRAM = 4;

    /**
     * Единица измерения - 1 штука
     */
    const UNIT_ONE_PIECE = 5;

    /**
     * Единица измерения - 100 миллилитров
     */
    const UNIT_100_MILLILITERS = 6;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_detail';
    }

    /**
     * Список единиц измерения для выпадающего списка
     * @return array
     */
    public static function getUnits()
    {
        return [
            self::UNIT_LITER           => 'Литр',
            self::UNIT_KILOGRAM        => 'Килограмм',
            self::UNIT_100_GRAM        => '100 грамм',
            self::UNIT_10_GRAM         => '10 грамм',
            self::UNIT_ONE_PIECE       => 'Штука',
            self::UNIT_100_MILLILITERS => '100 миллилитров'
        ];
    }

    /**
     * Название одного измерения
     *
     * @param $unit
     * @return mixed|string
     */
    public static function getUnitName($unit)
    {
        $units = self::getUnits();

        return isset($units[$unit]) ? $units[$unit] : 'Литр';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'product_card_id', 'units', 'price_per_one_unit'], 'required'],
            [['order_id', 'product_card_id', 'units', 'count', 'price_per_one_unit'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'product_card_id' => 'Продукт',
            'units' => 'Единицы измерения',
            'count' => 'Количество',
            'price_per_one_unit' => 'Цена за единицу',
            'created_at' => 'Создано',
            'updated_at' => 'Редактировано',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductCard::class, ['id' => 'product_card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::class, ['id' => 'order_id']);
    }
}
