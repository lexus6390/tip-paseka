<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Форма фильтрации заказов продуктов по датам.
 *
 * @property string $brand
 * @property string $model
 */
class ReportFilterForm extends Model
{
    /**
     * Дата начала отчета
     * @var string
     */
    public $dateStart;

    /**
     * Дата окончания отчета
     * @var string
     */
    public $dateEnd;

    /**
     * ReportFilterForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        // По умолчанию отчёт формируется с 1 апреля текущего года по текущий день
        $this->dateStart = date('Y-m-d', strtotime('1 april this year'));
        $this->dateEnd = date('Y-m-d', strtotime('today'));

        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateStart', 'dateEnd'], 'required'],
            [['dateStart', 'dateEnd'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dateStart' => 'Дата начала отчёта',
            'dateEnd'   => 'Дата окончания отчета'
        ];
    }
}