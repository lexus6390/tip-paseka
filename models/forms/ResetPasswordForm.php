<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Форма смены пароля админинстратора
 */
class ResetPasswordForm extends Model
{
    /**
     * @var string
     */
    public $password;

    /**
     * @var string
     */
    public $repeatPassword;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'repeatPassword'], 'required'],
            [['password', 'repeatPassword'], 'string', 'min' => 6],
            ['repeatPassword', 'compare', 'compareAttribute' => 'password'],
        ];
    }

    /**
     * @inheritdoc
     */
    public  function attributeLabels()
    {
        return [
            'password' => 'Пароль',
            'repeatPassword' => 'Повторите пароль'
        ];
    }

}