<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Форма для запроса смены пароля администратора
 */
class RequestChangePasswordForm extends Model
{
    /**
     * @var string
     */
    public $login;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login'], 'required'],
            [['login'], 'email']
        ];
    }

    public function attributeLabels()
    {
        return [
            'login' => 'Логин'
        ];
    }


}