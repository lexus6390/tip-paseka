<?php

namespace app\models\forms;

use yii\base\Model;

/**
 * Форма фильтрации заказов клиента по датам.
 *
 * @property string $brand
 * @property string $model
 */
class ReportClientFilterForm extends Model
{
    /**
     * Дата начала отчета
     * @var string
     */
    public $dateStart;

    /**
     * Дата окончания отчета
     * @var string
     */
    public $dateEnd;

    /**
     * Клиент
     * @var string
     */
    public $client;

    /**
     * ReportFilterForm constructor.
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        // По умолчанию отчёт формируется с 1 апреля текущего года по текущий день
        $this->dateStart = date('Y-m-d', strtotime('1 april this year'));
        $this->dateEnd = date('Y-m-d', strtotime('today'));

        parent::__construct($config);
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dateStart', 'dateEnd', 'client'], 'required'],
            [['dateStart', 'dateEnd', 'client'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dateStart' => 'Дата начала отчёта',
            'dateEnd'   => 'Дата окончания отчета',
            'client'    => 'Клиент'
        ];
    }
}