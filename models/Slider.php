<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $link
 * @property int $is_visible
 * @property int $priority
 * @property string $created_at
 * @property string $updated_at
 */
class Slider extends ActiveRecord
{
    /**
     * @var UploadedFile
     */
    public $image;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['priority'], 'required'],
            [['is_visible', 'priority'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['link', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'link'       => 'Изображение',
            'image'      => 'Изображение',
            'is_visible' => 'Отображать',
            'priority'   => 'Приоритет',
            'created_at' => 'Дата и время создания',
            'updated_at' => 'Дата и время редактирования',
        ];
    }
}
