<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property int $has_viber
 * @property string $viber_id
 * @property string $viber_name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property AdvertisementClient[] $advertisementClients
 * @property Order[] $orders
 */
class Client extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * Получение списка всех клиентов для выпадающего списка
     * @return array
     */
    public static function getClientList()
    {
        /** @var Client[] $clients */
        $clients = Client::find()->all();
        $clientList = [];
        foreach ($clients as $client) {
            $clientList[$client->id] = $client->getFullName().' ('.$client->phone.')';
        }

        return $clientList;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'phone'], 'required'],
            [['has_viber'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['first_name', 'middle_name', 'last_name', 'phone', 'email', 'viber_id', 'viber_name'], 'string', 'max' => 255],
            ['email', 'email']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'first_name'  => 'Имя',
            'middle_name' => 'Отчество',
            'last_name'   => 'Фамилия',
            'phone'       => 'Телефон',
            'email'       => 'Email',
            'has_viber'   => 'Есть Viber',
            'viber_id'    => 'Viber ID',
            'viber_name'  => 'Имя в Viber',
            'created_at'  => 'Создан',
            'updated_at'  => 'Редактирован',
        ];
    }

    /**
     * Получение полного ФИО клиента (при наличии)
     * @return string
     */
    public function getFullName()
    {
        $fullName = $this->first_name;

        if(!is_null($this->middle_name)) {
            $fullName .= ' '.$this->middle_name;
        }
        if(!is_null($this->last_name)) {
            $fullName .= ' '.$this->last_name;
        }

        return $fullName;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisementClients()
    {
        return $this->hasMany(AdvertisementClient::class, ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['client_id' => 'id']);
    }
}
