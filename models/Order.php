<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property int $season
 * @property int $client_id
 * @property int $total_price
 * @property int $is_delivery
 * @property string $delivery_address
 * @property string $day_of_purchase
 * @property int $is_preorder
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Client $client
 * @property OrderDetail $details
 */
class Order extends ActiveRecord
{
    /**
     * @var array
     */
    public $products;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * Получение доступных сезонов
     * @return array
     */
    public static function getAvailableSeasons()
    {
        return [
            '2020' => '2020'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'total_price'], 'required'],
            [['season','client_id', 'total_price', 'is_delivery', 'is_preorder'], 'integer'],
            [['day_of_purchase', 'created_at', 'updated_at'], 'safe'],
            [['delivery_address'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'               => 'ID',
            'season'           => 'Сезон',
            'client_id'        => 'Клиент',
            'total_price'      => 'Сумма заказа',
            'is_delivery'      => 'Есть доставка',
            'delivery_address' => 'Адрес доставки',
            'day_of_purchase'  => 'День покупки',
            'is_preorder'      => 'Это предзаказ?',
            'created_at'       => 'Дата создания',
            'updated_at'       => 'Дата редактирования',
        ];
    }

    /**
     * Получение деталей заказа
     */
    public function getProductDetails() : void
    {
        $details = $this->details;

        $result = [];
        foreach ($details as $detail) {
            $result[] = ['product' => $detail->product_card_id, 'count' => $detail->count];
        }

        $this->products = $result;
    }

    /**
     * Обновление суммы заказа при изменении состава заказа
     * @return int
     */
    public function updateTotalPrice()
    {
        $totalPrice = 0;

        $orderDetails = OrderDetail::find()
            ->where(['order_id' => $this->id])
            ->all();

        foreach ($orderDetails as $orderDetail) {
            $totalPrice += $orderDetail->price_per_one_unit * $orderDetail->count;
        }
        return $totalPrice;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetails()
    {
        return $this->hasMany(OrderDetail::class, ['order_id' => 'id']);
    }
}
