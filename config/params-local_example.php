<?php

return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=;dbname=',
        'username' => '',
        'password' => '',
        'charset' => 'utf8',
    ],
    'debug' => false,
    'domain' => '',
    'mailer_transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => '',
        'username' => '',
        'password' => '',
        'port' => '',
        'encryption' => ''
    ],
    'mail_from' => '',
    'mail_to' => '',
    'viber_api_key' => ''
];