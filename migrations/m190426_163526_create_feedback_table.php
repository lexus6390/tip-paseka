<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%feedback}}`.
 */
class m190426_163526_create_feedback_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%feedback}}', [
            'id'          => $this->primaryKey(),
            'name'        => $this->string()->notNull(),
            'last_name'   => $this->string(),
            'email'       => $this->string()->notNull(),
            'theme'       => $this->string(),
            'message'     => $this->text(),
            'is_read'     => $this->tinyInteger()->notNull()->defaultValue(0),
            'created_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%feedback}}');
    }
}
