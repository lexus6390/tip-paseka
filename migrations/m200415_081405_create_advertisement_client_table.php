<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advertisement_client}}`.
 */
class m200415_081405_create_advertisement_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advertisement_client}}', [
            'id'               => $this->primaryKey(),
            'client_id'        => $this->integer()->notNull(),
            'advertisement_id' => $this->integer()->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advertisement_client}}');
    }
}
