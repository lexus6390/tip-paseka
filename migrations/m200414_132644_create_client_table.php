<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client}}`.
 */
class m200414_132644_create_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client}}', [
            'id'          => $this->primaryKey(),
            'first_name'  => $this->string()->notNull(),
            'middle_name' => $this->string()->null(),
            'last_name'   => $this->string()->null(),
            'phone'       => $this->string()->notNull(),
            'email'       => $this->string()->null(),
            'has_viber'   => $this->boolean()->notNull()->defaultValue(false),
            'viber_id'    => $this->string()->null(),
            'viber_name'  => $this->string()->null(),
            'created_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client}}');
    }
}
