<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%database_category}}`.
 */
class m190427_072311_create_database_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%database_category}}', [
            'id'          => $this->primaryKey(),
            'title'       => $this->string(),
            'link'        => $this->string(),
            'priority'    => $this->integer()->notNull(),
            'created_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'  => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%database_category}}');
    }
}
