<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_detail}}`.
 */
class m200415_114308_create_order_detail_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_detail}}', [
            'id'                 => $this->primaryKey(),
            'order_id'           => $this->integer()->notNull(),
            'product_card_id'    => $this->integer()->notNull(),
            'units'              => $this->integer()->notNull(),
            'count'              => $this->integer()->notNull()->defaultValue(1),
            'price_per_one_unit' => $this->integer()->notNull(),
            'created_at'         => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'         => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_detail}}');
    }
}
