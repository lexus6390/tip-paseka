<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%advertisement}}`.
 */
class m200415_071259_create_advertisement_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%advertisement}}', [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(),
            'text'       => $this->text()->notNull(),
            'picture'    => $this->string()->null(),
            'is_draft'   => $this->boolean()->notNull()->defaultValue(true),
            'is_send'    => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
            'send_at'    => $this->timestamp()->null()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%advertisement}}');
    }
}
