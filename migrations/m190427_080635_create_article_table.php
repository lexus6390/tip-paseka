<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%article}}`.
 */
class m190427_080635_create_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%article}}', [
            'id'             => $this->primaryKey(),
            'category_id'    => $this->integer()->notNull(),
            'title'          => $this->string(),
            'description'    => $this->text(),
            'author'         => $this->string(),
            'text'           => $this->text(),
            'link'           => $this->string(),
            'is_published'   => $this->tinyInteger()->notNull()->defaultValue(0),
            'published_date' => $this->string(),
            'priority'       => $this->integer()->notNull(),
            'created_at'     => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'     => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%article}}');
    }
}
