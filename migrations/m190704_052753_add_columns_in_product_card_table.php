<?php

use yii\db\Migration;

/**
 * Class m190704_052753_add_columns_in_product_card_table
 */
class m190704_052753_add_columns_in_product_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('product_card', 'price_per', $this->tinyInteger());
        $this->addColumn('product_card', 'database_category_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('product_card', 'price_per');
        $this->dropColumn('product_card', 'database_category_id');
    }
}
