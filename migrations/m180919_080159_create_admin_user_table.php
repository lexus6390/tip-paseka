<?php

use app\models\AdminUser;
use yii\db\Migration;

/**
 * Handles the creation of table `admin_user`.
 */
class m180919_080159_create_admin_user_table extends Migration
{
    private $admins = [
        'alex@itlavka.com',
        'anton@itlavka.com'
    ];

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('admin_user', [
            'id'          => $this->primaryKey(),
            'login'       => $this->string(255)->notNull(),
            'secret'      => $this->string(255)->notNull(),
            'auth_key'    => $this->string(255)->notNull(),
            'reset_token' => $this->string(255)->defaultValue(null)
        ]);

        foreach ($this->admins as $admin) {
            $adminUser = new AdminUser();
            $adminUser->setAttributes([
                'login'  => $admin,
                'secret' => 'somepassword'
            ]);
            $adminUser->save();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('admin_user');
    }
}
