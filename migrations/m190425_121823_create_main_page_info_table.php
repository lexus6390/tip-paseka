<?php

use yii\db\Migration;

/**
 * Handles the creation of table `main_page_info`.
 */
class m190425_121823_create_main_page_info_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('main_page_info', [
            'id'                      => $this->primaryKey(),
            'phone'                   => $this->string(),
            'email'                   => $this->string(),
            'main_title'              => $this->string(),
            'main_text'               => $this->text(),
            'is_visible_main_text'    => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_visible_our_products' => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_visible_review'       => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_visible_database'     => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_visible_subscribe'    => $this->tinyInteger()->notNull()->defaultValue(0),
            'created_at'              => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'              => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('main_page_info');
    }
}
