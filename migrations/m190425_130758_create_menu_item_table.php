<?php

use yii\db\Migration;

/**
 * Handles the creation of table `menu_item`.
 */
class m190425_130758_create_menu_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('menu_item', [
            'id'         => $this->primaryKey(),
            'item_name'  => $this->string()->notNull(),
            'item_link'  => $this->string()->notNull(),
            'is_visible' => $this->tinyInteger()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('menu_item');
    }
}
