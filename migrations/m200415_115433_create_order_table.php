<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m200415_115433_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id'               => $this->primaryKey(),
            'season'           => $this->integer()->notNull(),
            'client_id'        => $this->integer()->notNull(),
            'total_price'      => $this->integer()->notNull()->defaultValue(0),
            'is_delivery'      => $this->boolean()->notNull()->defaultValue(false),
            'delivery_address' => $this->string()->null(),
            'day_of_purchase'  => $this->timestamp()->null(),
            'is_preorder'      => $this->boolean()->notNull()->defaultValue(false),
            'created_at'       => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'       => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}
