<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_card}}`.
 */
class m190426_065017_create_product_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_card}}', [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(),
            'short_description' => $this->text(),
            'full_description'  => $this->text(),
            'info'              => $this->text(),
            'is_visible'        => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_available'      => $this->tinyInteger()->notNull()->defaultValue(0),
            'is_pre_order'      => $this->tinyInteger()->notNull()->defaultValue(0),
            'date_available'    => $this->string(),
            'date_pre_order'    => $this->string(),
            'link'              => $this->string(),
            'price'             => $this->integer(),
            'priority'          => $this->integer(),
            'created_at'        => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at'        => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_card}}');
    }
}
