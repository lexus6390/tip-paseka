<?php

use yii\db\Migration;

/**
 * Handles the creation of table `slider`.
 */
class m190426_052001_create_slider_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('slider', [
            'id'         => $this->primaryKey(),
            'link'       => $this->string(),
            'is_visible' => $this->tinyInteger()->notNull()->defaultValue(0),
            'priority'   => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('slider');
    }
}
