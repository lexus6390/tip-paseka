<?php

namespace app\widgets;

use app\models\MainPageInfo;
use app\models\MenuItem;
use yii\base\Widget;
use yii\web\ServerErrorHttpException;

/**
 * Отображение футера
 */
class FooterWidget extends Widget
{
    /**
     * @var MainPageInfo
     */
    public $mainPageInfo;

    /**
     * Активный пункт меню
     *
     * @var string
     */
    public $active;

    public function run()
    {
        if(is_null($this->mainPageInfo)) {
            $this->mainPageInfo = MainPageInfo::find()->all()[0];
        }

        $menuItems = MenuItem::find()
            ->where(['is_visible' => 1])
            ->all();

        if(empty($menuItems)) {
            throw new ServerErrorHttpException('Не найдено ни одного пункта меню');
        }

        return $this->render('footer', [
            'mainPageInfo' => $this->mainPageInfo,
            'menuItems'    => $menuItems,
            'active'       => $this->active
        ]);
    }

}