<?php

namespace app\widgets;

use app\models\Article;
use yii\base\Widget;

/**
 * Виджет отображения других статей категории
 */
class OtherArticlesWidget extends Widget
{
    /**
     * @var Article
     */
    public $currentArticle;

    /**
     * @return string
     */
    public function run()
    {
        $articles = Article::find()
            ->where([
                'category_id' => $this->currentArticle->category_id,
                'is_published' => 1
            ])
            ->orderBy('priority ASC')
            ->all();

        return $this->render('other_articles', [
            'articles'       => $articles,
            'currentArticle' => $this->currentArticle
        ]);
    }
}