<?php

/** @var $sliderImages \app\models\Slider[] */

?>

<!-- Слайдер -->
<aside id="nnd-hero" style="margin-bottom: -50px;">
    <div class="flexslider">
        <ul class="slides" style="height: 450px">
            <?php foreach ($sliderImages as $sliderImage) { ?>
                <li style="background-image: url(<?= $sliderImage->link ?>); height: 100%; background-size: cover">
                    <div class="overlay"></div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-md-pull-2 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner"></div>
                            </div>
                        </div>
                    </div>
                </li>
            <?php } ?>
        </ul>
    </div>
</aside>
