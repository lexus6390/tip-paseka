<?php

use yii\helpers\Url;

/** @var $articles \app\models\Article[] */

$css = <<<CSS

.article-entry {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
.article-entry:hover{
 -webkit-transform: scale(1.05);
 -moz-transform: scale(1.05);
 -o-transform: scale(1.05);
 }

CSS;
$this->registerCss($css);

?>

<div class="nnd-blog">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 text-center nnd-heading">
                <h2><span>База знаний</span></h2>
            </div>
        </div>
        <div class="row">
            <?php foreach ($articles as $article) { ?>
                <div class="col-md-4">
                    <a href="<?= Url::toRoute(['note', 'id' => $article->id]) ?>">
                        <article class="article-entry">
                            <div class="blog-img" style="background-image: url(<?= $article->link ?>);"></div>
                            <div class="desc">
                                <p class="meta">
                                    <span class="month">
                                        <?= \Yii::$app->formatter->asDate($article->published_date, 'd MMMM YYYY') ?>
                                    </span>
                                </p>
                                <h2>
                                    <a href="<?= Url::toRoute(['note', 'id' => $article->id]) ?>">
                                        <?= $article->title ?>
                                    </a>
                                </h2>
                                <p><?= $article->description ?></p>
                            </div>
                        </article>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
