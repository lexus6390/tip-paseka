<?php

use yii\helpers\Url;

/** @var $products \app\models\ProductCard[] */
/** @var $currentProduct \app\models\ProductCard */

?>

<div class="col-md-2 col-md-pull-10">
    <div class="sidebar">
        <div class="side">
            <h2 class="text-center">Наша продукция</h2>
            <div class="fancy-collapse-panel">
                <div class="panel-group">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php foreach ($products as $product) { ?>
                                <?php if($product->title == $currentProduct->title) { ?>
                                    <h4 class="panel-body">
                                        <a href="<?= Url::toRoute(['product-detail', 'id' => $product->id]) ?>">
                                            <?= $product->title ?>
                                        </a>
                                    </h4>
                                <?php } else { ?>
                                    <h4 class="panel-body">
                                        <a class="collapsed" href="<?= Url::toRoute(['product-detail', 'id' => $product->id]) ?>">
                                            <?= $product->title ?>
                                        </a>
                                    </h4>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
