<?php

use yii\helpers\Url;

/** @var $articles \app\models\Article[] */
/** @var $currentArticle \app\models\Article */

?>

<div class="col-one-forth">
    <div class="row">
        <div class="col-md-12 about">
            <h4 class="text-center">Статьи категории</h4>
            <ul>
                <?php foreach ($articles as $article) { ?>
                    <?php if($article->id == $currentArticle->id) { ?>
                        <li class="text-center">
                            <a href="<?= Url::toRoute(['note', 'id' => $article->id]) ?>" style="color: #ee9537">
                                <b><?= $article->title ?></b>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li class="text-center">
                            <a href="<?= Url::toRoute(['note', 'id' => $article->id]) ?>">
                                <?= $article->title ?>
                            </a>
                        </li>
                    <?php } ?>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
