<?php

/** @var $mainPageInfo \app\models\MainPageInfo */
/** @var $menuItems \app\models\MenuItem[] */
/** @var $active string */

?>

<nav class="nnd-nav" role="navigation">
    <div class="top-menu">
        <div class="container">
            <div class="row">
                <div class="col-xs-2">
                    <div id="logo">
                        <a href="/">
                            <img src="images/logo.png" id="navigation-logo" alt="Landing Page"/>
                        </a>
                    </div>
                </div>
                <div class="col-xs-10 text-right menu-1 text-center">
                    <ul><li><a><i></i></a></li></ul>
                    <ul>
                        <?php foreach ($menuItems as $item) { ?>
                            <li class="<?= $item->item_name == $active ? 'active' : '' ?>">
                                <a href="<?= $item->item_link ?>">
                                    <?= mb_strtoupper($item->item_name) ?>
                                </a>
                            </li>
                        <?php } ?>
                        <li style="padding-right: 90px;"></li>
                        <li>
                            <span><i class="icon-phone"></i></span>
                            <a href="tel://<?= str_replace(' ', '',$mainPageInfo->phone) ?>">
                                <?= $mainPageInfo->phone ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
