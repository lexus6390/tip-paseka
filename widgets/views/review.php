<div id="nnd-testimony" class="nnd-light-grey">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center nnd-heading">
                <h2><span>Слово нашим покупателям</span></h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="owl-carousel2">
                    <div class="item">
                        <div class="testimony text-center">
                            <span class="img-user" style="background-image: url(images/person1.jpg);"></span>
                            <span class="user">Оксана</span>
                            <small>Тольяти</small>
                            <blockquote>
                                <p>Всё круто, мне понравилось, очень вкусно, куплю еще в следующем году!</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony text-center">
                            <span class="img-user" style="background-image: url(images/person2.jpg);"></span>
                            <span class="user">Михаил</span>
                            <small>Самара</small>
                            <blockquote>
                                <p>Очень качественный продукт, вся семья довольна.</p>
                            </blockquote>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimony text-center">
                            <span class="img-user" style="background-image: url(images/person3.jpg);"></span>
                            <span class="user">Вероника</span>
                            <small>Екатеренбург</small>
                            <blockquote>
                                <p>Преобретаю не первый год, качество и вкус - как всегда на высоте!</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>