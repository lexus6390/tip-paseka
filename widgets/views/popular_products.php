<?php

use app\models\ProductCard;
use yii\helpers\Url;

/** @var $products ProductCard[] */

$css = <<<CSS

.product-entry {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
.product-entry:hover{
 -webkit-transform: scale(1.05);
 -moz-transform: scale(1.05);
 -o-transform: scale(1.05);
 }

CSS;
$this->registerCss($css);

?>

<div class="nnd-shop">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3 text-center nnd-heading">
                <h2><span>Популярная продукция</span></h2>
                <p></p>
            </div>
        </div>
        <div class="row">
            <?php foreach ($products as $product) { ?>
                <div class="col-md-4 text-center">
                    <a href="<?= Url::toRoute(['product-detail', 'id' => $product->id]) ?>">
                        <div class="product-entry">
                            <div class="product-img" style="background-image: url(<?= $product->link ?>);"></div>
                            <div class="desc">
                                <h3><?= $product->title ?></h3>
                                <?php if($product->is_available) { ?>
                                    <p class="price"><span><?= $product->price ?> ₽ / <?= ProductCard::getTypePricePer($product->price_per) ?></span></p>
                                <?php } else { ?>
                                    <p class="price"><span>Нет в наличии</span></p>
                                <?php } ?>
                            </div>
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
