<?php

namespace app\widgets;

use app\models\MainPageInfo;
use app\models\MenuItem;
use yii\base\Widget;
use yii\web\ServerErrorHttpException;

/**
 * Виджет отображения пунктов меню
 */
class MenuWidget extends Widget
{
    /**
     * @var MainPageInfo
     */
    public $mainPageInfo;

    /**
     * Активный пункт меню
     *
     * @var string
     */
    public $active;

    /**
     * @return string
     * @throws ServerErrorHttpException
     */
    public function run()
    {
        if(is_null($this->mainPageInfo)) {
            $this->mainPageInfo = MainPageInfo::find()->all()[0];
        }

        $menuItems = MenuItem::find()
            ->where(['is_visible' => 1])
            ->all();

        if(empty($menuItems)) {
            throw new ServerErrorHttpException('Не найдено ни одного пункта меню');
        }

        return $this->render('menu', [
            'mainPageInfo' => $this->mainPageInfo,
            'menuItems'    => $menuItems,
            'active'       => $this->active
        ]);
    }

}