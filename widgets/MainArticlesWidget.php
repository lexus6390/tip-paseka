<?php

namespace app\widgets;

use app\models\Article;
use yii\base\Widget;
use yii\web\NotFoundHttpException;

/**
 * Виджет вывода статей на главной странице
 */
class MainArticlesWidget extends Widget
{
    /**
     * Лимит статей для вывода
     */
    const LIMIT_ARTICLES = 3;

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $articles = $articles = Article::find()
            ->where(['is_published' => 1])
            ->orderBy('priority ASC')
            ->limit(self::LIMIT_ARTICLES)
            ->all();

        if(empty($articles)) {
            throw new NotFoundHttpException('Не найдено ни одной опубликованной статьи');
        }

        return $this->render('main_articles', [
            'articles' => $articles
        ]);
    }
}