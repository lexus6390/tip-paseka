<?php

namespace app\widgets;

use app\models\ProductCard;
use yii\base\Widget;
use yii\web\NotFoundHttpException;

/**
 * Виджет вывода популярных продуктов на главной странице
 */
class PopularProductsWidget extends Widget
{
    /**
     * Лимит вывода продуктов на главной
     */
    const LIMIT_PRODUCTS = 3;

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $products = ProductCard::find()
            ->where(['is_visible' => 1])
            ->orderBy('priority ASC')
            ->limit(self::LIMIT_PRODUCTS)
            ->all();

        if(empty($products)) {
            throw new NotFoundHttpException('Не найдено ни одного продукта');
        }

        return $this->render('popular_products', [
            'products' => $products
        ]);
    }

}