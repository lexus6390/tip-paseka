<?php

namespace app\widgets;

use app\models\Slider;
use yii\base\Widget;

/**
 * Виджет для отображения слайдера на главной странице
 */
class SliderWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        $sliderImages = Slider::find()
            ->where(['is_visible' => 1])
            ->orderBy('priority ASC')
            ->all();

        return $this->render('slider', [
            'sliderImages' => $sliderImages
        ]);
    }

}