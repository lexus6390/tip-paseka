<?php

namespace app\widgets;

use yii\base\Widget;

/**
 * Виджет отображения отзывов
 */
class ReviewWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('review');
    }
}