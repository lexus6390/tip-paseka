<?php


namespace app\widgets;

use app\models\ProductCard;
use yii\base\Widget;
use yii\web\NotFoundHttpException;

/**
 * Виджет продукции для страницы карточки продукта
 */
class ProductsWidget extends Widget
{
    /**
     * @var ProductCard
     */
    public $product;

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $products = ProductCard::find()
            ->where(['is_visible' => 1])
            ->orderBy('priority ASC')
            ->all();

        if(empty($products)) {
            throw new NotFoundHttpException('Не найдено ни одного продукта');
        }

        return $this->render('products', [
            'products'       => $products,
            'currentProduct' => $this->product
        ]);
    }

}