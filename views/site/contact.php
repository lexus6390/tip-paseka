<?php

use app\widgets\FooterWidget;
use app\widgets\MenuWidget;

/** @var $this yii\web\View */
/** @var $mainPageInfo \app\models\MainPageInfo */
/** @var $feedback \app\models\Feedback */

$this->title = 'Контакты';

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'Контакты'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-1.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7>Контакты</h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="nnd-contact">
        <div class="container">
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>Контакты</span>
            </h4>
            <br><br>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row contact-info-wrap">
                        <div class="col-md-4">
                            <p><span><i class="icon-phone"></i></span> <a href="tel://<?= str_replace(' ', '',$mainPageInfo->phone) ?>"><?= $mainPageInfo->phone ?></a></p>
                        </div>
                        <div class="col-md-4">
                            <p><span><i class="icon-mail2"></i></span>
                                <a href="mailto:<?= $mainPageInfo->email ?>"><?= $mainPageInfo->email ?></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-10 col-md-offset-1">
                    <div class="contact-wrap">
                        <h3>Обратная связь</h3>
                        <form action="/contact" method="post">
                            <div class="row form-group">
                                <div class="col-md-6 padding-bottom">
                                    <label for="fname">Имя*</label>
                                    <input name="name" type="text" id="fname" class="form-control" placeholder="Ваше имя">
                                </div>
                                <div class="col-md-6">
                                    <label for="lname">Фамилия</label>
                                    <input name="last_name" type="text" id="lname" class="form-control" placeholder="Ваша фамилия">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="email">Email*</label>
                                    <input name="email" type="text" id="email" class="form-control" placeholder="Введите Ваш Email">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="subject">Тема</label>
                                    <input name="theme" type="text" id="subject" class="form-control" placeholder="Введите тему сообщения">
                                </div>
                            </div>

                            <div class="row form-group">
                                <div class="col-md-12">
                                    <label for="message">Сообщение*</label>
                                    <textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Введите Ваше сообщение"></textarea>
                                </div>
                            </div>

                            <div class="errors"></div>

                            <div class="form-group text-center">
                                <input id="send" type="submit" value="Отправить" class="btn btn-primary">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'Контакты'
    ]) ?>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>

<!-- Модальное окно разрыва соединения по таймауту -->
<div id="modal_success" class="modal_form_success">
    <h3 class="text-center" style="margin-top: 35px;">
        Сообщение успешно отправлено
    </h3>
    <br>
    <div style="margin: 0 auto">
        <button type="submit" id="close_modal" class="redirect_success">Закрыть</button>
    </div>
</div>

<!-- Пoдлoжкa -->
<div id="overlay_success"></div>