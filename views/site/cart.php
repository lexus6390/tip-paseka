<div class="nnd-loader"></div>

<div id="page">
    <nav class="nnd-nav" role="navigation">
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-2">
                        <div id="logo">
                            <img src="images/logo.png" id="navigation-logo" alt="Landing Page"/>
                        </div>
                    </div>
                    <div class="col-xs-10 text-right menu-1">
                        <ul><li><a><i></i></a></li></ul>
                        <ul>
                            <li><a href="index.html">ГЛАВНАЯ</a></li>
                            <li><a href="shop.html">МАГАЗИН</a></li>
                            <li><a href="category.html">БАЗА ЗНАНИЙ</a></li>
                            <li><a href="contact.html">КОНТАКТЫ</a></li>
                            <li><a href="404.html">О НАС</a></li>
                            <li class="active"><a href="cart.html"><i class="icon-shopping-cart"></i> КОРЗИНА [0]</a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                            <li><span><i class="icon-phone"></i></span> <a href="tel://88008888888">8 800 888 88 88</a></li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </nav>
    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-3.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7>Ваша корзина</h7>

                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="nnd-shop">
        <div class="container">
            <h4 class="bread"><span><a href="index.html">Главная ></a></span> <span>Корзина</span></h4>
            <div class="row row-pb-md">
                <div class="col-md-10 col-md-offset-1">

                    <div class="process-wrap">
                        <div class="process text-center active">
                            <p><span>1</span></p>
                            <h3>Корзина</h3>
                        </div>
                        <div class="process text-center">
                            <p><span>2</span></p>
                            <h3>Детали заказа</h3>
                        </div>
                        <div class="process text-center">
                            <p><span>3</span></p>
                            <h3>Финиш</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row row-pb-md">
                <div class="col-md-10 col-md-offset-1">
                    <div class="product-name">
                        <div class="one-forth text-center">
                            <span>Наименование</span>
                        </div>
                        <div class="one-eight text-center">
                            <span>Цена</span>
                        </div>
                        <div class="one-eight text-center">
                            <span>Количество</span>
                        </div>
                        <div class="one-eight text-center">
                            <span>Итого</span>
                        </div>
                        <div class="one-eight text-center">
                            <span>Удалить</span>
                        </div>
                    </div>
                    <div class="product-cart">
                        <div class="one-forth">
                            <div class="product-img" style="background-image: url(images/item-1.jpg);">
                            </div>
                            <div class="display-tc">
                                <h3>Мёд</h3>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <input type="text" id="quantity" name="quantity" class="form-control input-number text-center" value="1" min="1" max="100">
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <a href="#" class="closed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="product-cart">
                        <div class="one-forth">
                            <div class="product-img" style="background-image: url(images/item-2.jpg);">
                            </div>
                            <div class="display-tc">
                                <h3>Еще мёд</h3>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <form action="#">
                                    <input type="text" name="quantity" class="form-control input-number text-center" value="1" min="1" max="100">
                                </form>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <a href="#" class="closed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="product-cart">
                        <div class="one-forth">
                            <div class="product-img" style="background-image: url(images/item-4.jpg);">
                            </div>
                            <div class="display-tc">
                                <h3>И еще мёд</h3>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <input type="text" id="quantity" name="quantity" class="form-control input-number text-center" value="1" min="1" max="100">
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">300 ₽</span>
                            </div>
                        </div>

                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <a href="#" class="closed"></a>
                            </div>
                        </div>
                    </div>
                    <div class="product-cart">
                        <div class="one-forth">
                            <div class="product-img" style="background-image: url(images/item-0.jpg);">
                            </div>
                            <div class="display-tc">
                                <h3>Доставка</h3>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">200 ₽</span>
                            </div>
                        </div>
                    </div>
                    <div class="product-cart">
                        <div class="one-forth">
                            <div>
                            </div>
                            <div class="display-tc">
                                <h3>Сумма к оплате</h3>
                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">

                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">

                            </div>
                        </div>
                        <div class="one-eight text-center">
                            <div class="display-tc">
                                <span class="price">1100 ₽</span>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="nnd-shop">
                <div class="container">
                    <div class="row row-pb-md">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="process-wrap">
                                <div class="process text-center active">
                                    <p><span>1</span></p>
                                    <h3>Корзина</h3>
                                </div>
                                <div class="process text-center active">
                                    <p><span>2</span></p>
                                    <h3>Детали заказа</h3>
                                </div>
                                <div class="process text-center">
                                    <p><span>3</span></p>
                                    <h3>Финиш</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-10 col-md-offset-1">
                            <form method="post" class="nnd-form">
                                <h2>Контакты</h2>
                                <div class="row">

                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="fname">Имя</label>
                                            <input type="text" id="fname" class="form-control" placeholder="Ваше имя">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="lname">Фамилия</label>
                                            <input type="text" id="lname" class="form-control" placeholder="Ваша фамилия">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="email">E-mail</label>
                                            <input type="text" id="email" class="form-control" placeholder="Введите E-mail">
                                        </div>
                                        <div class="col-md-6">
                                            <label for="Phone">Номер телефона</label>
                                            <input type="text" id="zippostalcode" class="form-control" placeholder="+X-XXX-XXX-XX-XX">
                                        </div>
                                    </div>
                                    <div>



                                        <p>Для самовывоза дальнейшее заполнение не обязательно <p>
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <label for="country">Выберете Страну</label>
                                                <div class="form-field ">
                                                    <i class="icon icon-arrow-down3"></i>
                                                    <select name="people" id="people" class="form-control">
                                                        <option value="#">Россия</option>
                                                        <option value="#">Белорусия</option>
                                                        <option value="#">Украина</option>
                                                        <option value="#">Алжир</option>
                                                        <option value="#">Антарктида</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="stateprovince">Регион</label>
                                                <input type="text" id="fname" class="form-control" placeholder="Ваш регион">
                                            </div>

                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label for="lname">Индекс</label>
                                            <input type="text" id="zippostalcode" class="form-control" placeholder="Ваш индекс">
                                        </div>



                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="companyname">Город</label>
                                                <input type="text" id="towncity" class="form-control" placeholder="Ваш город">
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="fname">Адрес</label>
                                                <input type="text" id="address" class="form-control" placeholder="Ваш адрес">
                                            </div>
                                            <div class="checkbox text-center">
                                                <label><input type="checkbox" value="">Я даю своё согласие на обработку персональных данных</label>
                                            </div>
                                        </div>
                                    </div>
                            </form>
                        </div></div>
                    <div class="col-md-10 col-md-offset-1">

                        <div class="cart-detail">
                            <h2>Метод оплаты</h2>
                            <div class="text-center">


                                <div class="process">

                                    <h3><input type="radio" name="optradio">  Камни</h3>
                                </div>
                                <div class="process">

                                    <h3><input type="radio" name="optradio">  Paypal</h3>
                                </div>
                                <div class="process">

                                    <h3><input type="radio" name="optradio">  Сбербанк</h3>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p><a href="order-complete.html" class="btn btn-primary" >Оформить заказ</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="nnd-shop">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3 text-center nnd-heading">
                    <h2><span>Ркомендуемые товары</span></h2>
                    <p>Возможно вы также захотите приобрести это.</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 text-center">
                    <div class="product-entry">
                        <div class="product-img" style="background-image: url(images/item-1.jpg);">
                            <p class="tag"><span class="new">НОВИНКА</span></p>
                            <div class="cart">
                                <p>
                                    <span class="addtocart"><a href="#"><i class="icon-shopping-cart"></i></a></span>
                                    <span><a href="product-detail.html"><i class="icon-eye"></i></a></span>
                                    <span><a href="#"><i class="icon-heart3"></i></a></span>
                                    <span><a href="404.html"><i class="icon-bar-chart"></i></a></span>
                                </p>
                            </div>
                        </div>
                        <div class="desc">
                            <h3><a href="shop.html">Мёд</a></h3>
                            <p class="price"><span>300 ₽</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="product-entry">
                        <div class="product-img" style="background-image: url(images/item-2.jpg);">
                            <p class="tag"><span class="new">НОВИНКА</span></p>
                            <div class="cart">
                                <p>
                                    <span class="addtocart"><a href="#"><i class="icon-shopping-cart"></i></a></span>
                                    <span><a href="product-detail.html"><i class="icon-eye"></i></a></span>
                                    <span><a href="#"><i class="icon-heart3"></i></a></span>
                                    <span><a href="404.html"><i class="icon-bar-chart"></i></a></span>
                                </p>
                            </div>
                        </div>
                        <div class="desc">
                            <h3><a href="shop.html">Мёд</a></h3>
                            <p class="price"><span>300 ₽</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="product-entry">
                        <div class="product-img" style="background-image: url(images/item-3.jpg);">
                            <p class="tag"><span class="new">НОВИНКА</span></p>
                            <div class="cart">
                                <p>
                                    <span class="addtocart"><a href="#"><i class="icon-shopping-cart"></i></a></span>
                                    <span><a href="product-detail.html"><i class="icon-eye"></i></a></span>
                                    <span><a href="#"><i class="icon-heart3"></i></a></span>
                                    <span><a href="404.html"><i class="icon-bar-chart"></i></a></span>
                                </p>
                            </div>
                        </div>
                        <div class="desc">
                            <h3><a href="shop.html">Мёд</a></h3>
                            <p class="price"><span>300 ₽</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="product-entry">
                        <div class="product-img" style="background-image: url(images/item-4.jpg);">
                            <p class="tag"><span class="new">НОВИНКА</span></p>
                            <div class="cart">
                                <p>
                                    <span class="addtocart"><a href="#"><i class="icon-shopping-cart"></i></a></span>
                                    <span><a href="product-detail.html"><i class="icon-eye"></i></a></span>
                                    <span><a href="#"><i class="icon-heart3"></i></a></span>
                                    <span><a href="404.html"><i class="icon-bar-chart"></i></a></span>
                                </p>
                            </div>
                        </div>
                        <div class="desc">
                            <h3><a href="shop.html">Мёд</a></h3>
                            <p class="price"><span>300 ₽</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div id="nnd-subscribe">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="col-md-6 text-center">
                        <h2><i class="icon-paperplane"></i>Быть в курсе новостей</h2>
                    </div>
                    <div class="col-md-6">
                        <form class="form-inline qbstp-header-subscribe">
                            <div class="row">
                                <div class="col-md-10 col-md-offset-1 col-md-offset-0">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="email" placeholder="Email">
                                        <button type="submit" class="btn btn-primary">Подписаться</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer id="nnd-footer" role="contentinfo">
        <nav class="nnd-nav">
            <div class="menu-1">
                <div class="container menu-2">
                    <div class="row">

                        <div class="col-xs-2">
                            <div id="logo">
                                <img src="images/logo.png" id="navigation-logo" alt="Landing Page"/>
                            </div>
                        </div>
                        <div class="col-xs-10 text-right ">
                            <ul>
                                <li><a><i class="icon-phone"></i></a></li>
                                <li><a><i></i> 8 800 888 88 88 </a></li>
                                <ul></ul><li><a href="index.html">ГЛАВНАЯ</a></li>
                                <li><a href="shop.html">МАГАЗИН</a></li>
                                <li><a href="category.html">БАЗА ЗНАНИЙ</a></li>
                                <li><a href="contact.html">КОНТАКТЫ</a></li>
                                <li><a href="404.html">О НАС</a></li>
                                <li class="active"><a href="cart.html"><i class="icon-shopping-cart"></i> КОРЗИНА [0]</a></li>
                                <li><a href="#"><i class="icon-vk"></i></a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <li><a href="#"><i class="icon-facebook"></i></a></li>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                                <li><a href="#"><i class="icon-instagram"></i></a></li>



                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="copy">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <p>
                        <span class="block">  <p id="copyright" class="col-2">Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script>  |  All rights reserved  |   <a href="http://nn-designer.pro/"> by NN Designer </a></p></span>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>