<?php

use app\models\ProductCard;
use app\widgets\FooterWidget;
use app\widgets\MenuWidget;
use app\widgets\ProductsWidget;
use yii\helpers\Url;

/** @var $product \app\models\ProductCard */

$this->title = $product->title;

$css = <<<CSS

.product-img {
    cursor: pointer;
}

.product-img {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
/*.product-img:hover{*/
 /*-webkit-transform: scale(1.5);*/
 /*-moz-transform: scale(1.5);*/
 /*-o-transform: scale(1.5);*/
 /*}*/

CSS;
$this->registerCss($css);

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'Продукция'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-2.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7><?= $product->title ?></h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="nnd-shop">
        <div class="container">

            <!-- Хлебные крошки -->
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>
                    <a href="/products">Продукция ></a>
                </span>
                <span><?= $product->title ?></span>
            </h4>

            <!-- Карточка продукта -->
            <div class="row">
                <div class="col-md-10 col-md-push-2">
                    <div class="product-detail-wrap">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="product-entry">
                                    <div class="product-img" style="background-image: url(<?= $product->link ?>);">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="desc">
                                    <h3><?= $product->title ?></h3>
                                    <p class="price">
                                        <span><?= $product->price ?> ₽ / <?= ProductCard::getTypePricePer($product->price_per) ?></span>
                                        <?php if(!$product->is_available) { ?>
                                            <br><br>
                                            <span>Нет в наличии</span>
                                            <br><br>
                                            <?php if(!is_null($product->date_available) && $product->date_available != '') { ?>
                                                <span style="width: 100%;text-decoration: underline">Ожидается: </span>
                                                <span style="width: 100%"><?= $product->date_available ?></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </p>
                                    <?php if(!$product->is_available && !$product->is_pre_order) { ?>
                                        <p class="price">
                                            <?php if(!is_null($product->date_pre_order) && $product->date_pre_order != '') { ?>
                                                <span style="width: 100%;text-decoration: underline">Предзаказ будет доступен:</span>
                                                <span style="width: 100%"><i><?= $product->date_pre_order ?></i></span>
                                            <?php } ?>
                                        </p>
                                    <?php } ?>
                                    <?php if($product->is_pre_order) { ?>
                                        <p class="price">
                                            <span>Доступен предзаказ</span>
                                        </p>
                                    <?php } ?>
                                    <p><?= $product->short_description ?></p>
                                </div>

                            </div>

                        </div>
                        <div class="row">

                        </div>
                    </div>
                    <div class="row">

                    </div>

                    <!-- Полное описание и инфо о товаре -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10 tabulation">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#description">Описание</a></li>
                                    <?php if(!is_null($product->info)) { ?>
                                        <li><a data-toggle="tab" href="#manufacturer">Информация о сборе</a></li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content">
                                    <div id="description" class="tab-pane fade in active">
                                        <p><?= $product->full_description ?></p>
                                    </div>
                                    <?php if(!is_null($product->info)) { ?>
                                        <div id="manufacturer" class="tab-pane fade">
                                            <p>
                                                <?php foreach ((array)json_decode($product->info) as $item) { ?>
                                                    <b><?= $item->key ?></b>: <?= $item->value ?><br>
                                                <?php } ?>
                                            </p>
                                        </div>
                                    <?php } ?>
                                </div>

                                <?php if(!is_null($product->database_category_id)) { ?>
                                    <br>
                                    <div class="text-center">
                                        <h5>Подробнее об этом продукте читайте в наших статьях</h5>
                                        <a href="<?= Url::toRoute(['articles', 'id' => $product->database_category_id]) ?>" class="btn btn-primary" target="_blank">
                                            Перейти к статьям
                                        </a>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- Фильтры/спиок категорий товаров -->
                <?php if(!\Yii::$app->devicedetect->isMobile()) {
                    echo ProductsWidget::widget([
                        'product' => $product
                    ]);
                } ?>

            </div>
        </div>

        <br><br>

        <!-- Футер -->
        <?= FooterWidget::widget([
            'active' => 'Продукция'
        ]) ?>

    </div>

    <!-- Модальное окно разрыва соединения по таймауту -->
    <div id="modal_image" class="modal_form">
        <h3 class="text-center" style="margin-top: 35px;">
            <?= $product->title ?>
        </h3>
        <br>
        <img src="<?= $product->link ?>" style="width: 100%;" alt="">
        <br><br>
        <div style="margin: 0 auto">
            <button type="submit" id="close_modal_image" class="redirect">Закрыть</button>
        </div>
    </div>

    <!-- Пoдлoжкa -->
    <div id="overlay"></div>

</div>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>