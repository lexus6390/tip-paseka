<?php

use app\widgets\FooterWidget;
use app\widgets\MenuWidget;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $articles \app\models\Article[] */

$this->title = $articles[0]->category->title;

$css = <<<CSS

p {
    color: #595959;!important;
}

.article-entry {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
.article-entry:hover{
 -webkit-transform: scale(1.05);
 -moz-transform: scale(1.05);
 -o-transform: scale(1.05);
 }

CSS;
$this->registerCss($css);

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'База знаний'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(<?= $articles[0]->category->link ?>);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7><?= $articles[0]->category->title ?></h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="nnd-blog">
        <div class="container">
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>
                    <a href="/database">База знаний ></a>
                </span>
                <span><?= $articles[0]->category->title ?></span>
            </h4>
            <div class="row">
                <?php foreach ($articles as $article) { ?>
                    <div class="col-md-4">
                        <a href="<?= Url::toRoute(['note', 'id' => $article->id]) ?>" >
                            <article class="article-entry">
                                <div class="blog-img" style="background-image: url(<?= $article->link ?>);"></div>
                                <div class="desc">
                                    <p class="meta">
                                        <span class="month">
                                            <?= \Yii::$app->formatter->asDate($article->published_date, 'd MMMM YYYY') ?>
                                        </span>
                                    </p>
                                    <h2><?= $article->title ?></h2>
                                    <p><?= $article->description ?></p>
                                </div>
                            </article>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'База знаний'
    ]) ?>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>