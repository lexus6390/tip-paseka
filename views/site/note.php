<?php

use app\widgets\FooterWidget;
use app\widgets\MenuWidget;
use app\widgets\OtherArticlesWidget;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $article \app\models\Article */

$this->title = $article->title;

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'База знаний'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(<?= $article->link ?>);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7><?= $article->title ?></h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div id="nnd-about">
        <div class="container">
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>
                    <a href="/database">База знаний ></a>
                </span>
                <span>
                    <a href="<?= Url::toRoute(['articles', 'id' => $article->category_id]) ?>"> <?= $article->category->title ?> ></a>
                </span>
                <span><?= $article->title ?></span>
            </h4>
            <div class="row">
                <div class="about-flex">

                    <!-- Все статьи категории -->
                    <?= OtherArticlesWidget::widget([
                        'currentArticle' => $article
                    ]) ?>

                    <div class="col-three-forth">
                        <h2><?= $article->title ?></h2>
                        <h6><i>Автор: <?= $article->author ?></i></h6>
                        <h6><i>Дата публикации: <?= $article->published_date ?></i></h6>
                        <div class="row">
                            <div class="col-md-12">
                                <?= $article->text ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'База знаний'
    ]) ?>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>