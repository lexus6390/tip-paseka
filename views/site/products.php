<?php

use app\models\ProductCard;
use app\widgets\FooterWidget;
use app\widgets\MenuWidget;
use yii\helpers\Url;

/** @var $products \app\models\ProductCard[] */

$this->title = 'Продукция';

$css = <<<CSS

.product-entry {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
.product-entry:hover{
 -webkit-transform: scale(1.05);
 -moz-transform: scale(1.05);
 -o-transform: scale(1.05);
 }

CSS;
$this->registerCss($css);

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'Продукция'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-2.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7>Продукция</h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="nnd-shop">
        <div class="container">

            <!-- Хлебные крошки -->
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>Продукция</span>
            </h4>

            <div class="row">
                <div class="col-md-12">
                    <div class="row row-pb-lg">
                        <?php foreach ($products as $product) { ?>
                            <div class="col-md-3 text-center">
                                <a href="<?= Url::toRoute(['product-detail', 'id' => $product->id]) ?>">
                                    <div class="product-entry">
                                        <div class="product-img" style="background-image: url(<?= $product->link ?>);"></div>
                                        <div class="desc">
                                            <h3>
                                                <a href="<?= Url::toRoute(['product-detail', 'id' => $product->id]) ?>">
                                                    <?= $product->title ?>
                                                </a>
                                            </h3>
                                            <?php if($product->is_available) { ?>
                                                <p class="price"><span><?= $product->price ?> ₽ / <?= ProductCard::getTypePricePer($product->price_per) ?></span></p>
                                            <?php } else { ?>
                                                <p class="price"><span>Нет в наличии</span></p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'Продукция'
    ]) ?>
</div>


<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>