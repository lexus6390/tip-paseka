<?php

use app\widgets\MenuWidget;

/* @var $this yii\web\View */
/* @var $exception Exception */
/* @var $statusCode string */
/* @var $name string */
/* @var $message string */

$this->title = $name;
?>

<div class="nnd-loader"></div>

<div id="page">

    <?= MenuWidget::widget() ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-404.jpg);">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7><?= $message ?></h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>


    <div class="nnd-shop">
        <div class="container">

            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <h8>#<?= $statusCode ?></h8>
                    <p>
                        <a href="/" class="btn btn-primary">
                            На главную
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>

    <footer id="nnd-footer" role="contentinfo">

        <div class="copy">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <p>
                        <span class="block">  <p id="copyright" class="col-2">Copyright &copy; 2015-<script>document.write(new Date().getFullYear());</script>  |  All rights reserved  |   <a href="http://nn-designer.pro/"> by NN Designer </a></p></span>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
