<?php

use app\widgets\FooterWidget;
use app\widgets\MenuWidget;
use yii\helpers\Url;

/** @var $this yii\web\View */
/** @var $categories \app\models\DatabaseCategory[] */

$this->title = 'База знаний';

$colmdArray = [0,3,4,7,8,11,12,15,16];

$css = <<<CSS

.article-entry {
 -moz-transition: all 1s ease-out;
 -o-transition: all 1s ease-out;
 -webkit-transition: all 1s ease-out;
 }
 
.article-entry:hover{
 -webkit-transform: scale(1.05);
 -moz-transform: scale(1.05);
 -o-transform: scale(1.05);
 }

CSS;
$this->registerCss($css);

?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'active' => 'База знаний'
    ]) ?>

    <aside id="nnd-hero" class="breadcrumbs">
        <div class="flexslider">
            <ul class="slides">
                <li style="background-image: url(images/cover-img-1.jpg);">

                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 col-sm-12 col-xs-12 slider-text">
                                <div class="slider-text-inner text-center">
                                    <h7>База знаний</h7>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </aside>

    <div class="nnd-blog">
        <div class="container">
            <h4 class="bread">
                <span>
                    <a href="/">Главная ></a>
                </span>
                <span>База знаний</span>
            </h4>
            <div class="row">
                <?php foreach ($categories as $key => $category) { ?>
                    <?php $colMd = 4;

                    if(in_array($key, $colmdArray)) {
                        $colMd = 8;
                    }
                    ?>

                    <div class="col-md-<?= $colMd ?>">
                        <a href="<?= Url::toRoute(['articles', 'id' => $category->id]) ?>">
                            <article class="article-entry">
                                <div class="blog-img" style="background-image: url(<?= $category->link ?>)"></div>
                                <div class="desc">
                                    <p class="meta">
                                        <span class="month" style="font-size: 20px;">
                                            <?= $category->title ?>
                                        </span>
                                    </p>
                                </div>
                            </article>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'База знаний'
    ]) ?>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>