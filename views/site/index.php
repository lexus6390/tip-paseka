<?php

use app\widgets\FooterWidget;
use app\widgets\MainArticlesWidget;
use app\widgets\MenuWidget;
use app\widgets\PopularProductsWidget;
use app\widgets\ReviewWidget;
use app\widgets\SliderWidget;

/** @var $this yii\web\View */
/** @var $mainPageInfo \app\models\MainPageInfo */

$this->title = 'Пасека Типочкиных';
?>

<div class="nnd-loader"></div>

<div id="page">

    <!-- Верхнее меню -->
    <?= MenuWidget::widget([
        'mainPageInfo' => $mainPageInfo,
        'active' => 'Главная'
    ]) ?>

    <!-- Слайдер -->
    <?= SliderWidget::widget() ?>

    <!-- Приветственный блок -->
    <?php if($mainPageInfo->is_visible_main_text) { ?>
        <div class="nnd-blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2 text-center nnd-heading">
                        <h2>
                            <span><?= $mainPageInfo->main_title ?></span>
                        </h2>
                        <p><?= $mainPageInfo->main_text ?></p>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Блок "Наша продукция" -->
    <?php if($mainPageInfo->is_visible_our_products) {
        echo PopularProductsWidget::widget();
    } ?>

    <!-- Блок "Отзывы -->
    <?php if($mainPageInfo->is_visible_review) {
        echo ReviewWidget::widget();
    } ?>

    <!-- Блок "База знаний" -->
    <?php if($mainPageInfo->is_visible_database) {
        echo MainArticlesWidget::widget();
     } ?>

    <!-- Блок "Подписка" -->
    <?php if($mainPageInfo->is_visible_subscribe) { ?>
        <div id="nnd-subscribe">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="col-md-6 text-center">
                            <h2><i class="icon-paperplane"></i>Быть в курсе новостей</h2>
                        </div>
                        <div class="col-md-6">
                            <form class="form-inline qbstp-header-subscribe">
                                <div class="row">
                                    <div class="col-md-12 col-md-offset-0">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="email" placeholder="Email">
                                            <button type="submit" class="btn btn-primary">Подписаться</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

    <!-- Футер -->
    <?= FooterWidget::widget([
        'active' => 'Главная'
    ]) ?>

</div>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>